import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify'
import 'toastr/build/toastr.min.css';
import 'typeface-roboto/index.css';
import axios from 'axios';
import toastr from 'toastr';

axios.defaults.withCredentials = true;
axios.defaults.baseURL = '/training/backend/api/v1';
axios.interceptors.response.use((response) => response, error => {
  if (error.response && error.response.data.user_message) {
    toastr.error(error.response.data.user_message);
  } else {
    toastr.error(`${error.message} ("${error.request.statusText.toLowerCase()}")`, 'Unexpected error');
  }
  return Promise.reject(error);
});

Vue.config.productionTip = false

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')

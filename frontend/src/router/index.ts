import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'
import DefaultLayout from '../layouts/Default.vue';
import '../register-hooks';
import store from '../store/';

Vue.use(VueRouter)

const routes: Array<RouteConfig> = [
  {
    path: '/',
    component: DefaultLayout,
    children: [
      {
        path: '/',
        name: 'root',
        beforeEnter: (to, from, next) => {
          if (to.path === '/') {
            store.dispatch('users/getCurrentUser').then((userId) => {
              store.dispatch('users/getUserClassrooms', userId).then((classrooms) => {
                if (classrooms.length) {
                  next({ path: `/student/${userId}/classroom/${classrooms[0]}` });
                } else {
                  next('/trainings');
                }
              });
            });
          }
        },
      },
      {
        path: '/trainings',
        name: 'trainings',
        component: () =>
            import(/* webpackChunkName: "trainings" */ '../views/Trainings.vue'),
      },
      {
        path: '/classroom/:classroomId',
        props: true,
        name: 'classroom',
        component: () =>
            import(/* webpackChunkName: "classroom" */ '../views/Classroom.vue'),
      },
      {
        path: '/student/:userId/classroom/:classroomId',
        alias: '/classroom/:classroomId/student/:userId',
        props: (route) => {
          const userId = route.params.userId;
          const classroomId = parseInt(route.params.classroomId, 10);
          return { userId, classroomId };
        },
        name: 'studentClassroom',
        component: () =>
            import(/* webpackChunkName: "student" */ '../views/StudentClassroom.vue'),
      },
      {
        path: '/student/:userId/classroom/:classroomId/edit',
        alias: '/classroom/:classroomId/student/:userId/edit',
        props: (route) => {
          const userId = route.params.userId;
          const classroomId = parseInt(route.params.classroomId, 10);
          return { userId, classroomId };
        },
        name: 'studentClassroomEdit',
        component: () =>
            import(/* webpackChunkName: "studentEdit" */ '../views/StudentClassroomEdit.vue'),
      },
    ],
  },
]

const router = new VueRouter({
  routes
})

export default router

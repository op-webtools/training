import Vue from 'vue';
import axios from 'axios';

// Mutations
const SET_ALL_GROUPS = 'SET_ALL_GROUPS';
const SET_GROUP_TRAININGS = 'SET_GROUP_TRAININGS';
const DELETE_GROUP = 'DELETE_GROUP';

const state = {
  all: null,
};

const getters = {
  trainingGroupsArray: (state) => {
    if (state.all) {
      return Object.values(state.all);
    }
  },
};

const actions = {
  getAllGroups({ commit, state }) {
    return axios
      .get(`/groups`, { withCredentials: true })
      .then((response) => {
        const data = response.data;
        const tmp = {};
        for (const k of data) {
          tmp[k.id] = k;
        }
        commit(SET_ALL_GROUPS, tmp);
        return tmp;
      })
      .catch((error) => {
        return Promise.reject(new Error(error));
      });
  },
  updateGroupTrainingOrder({ commit }, { groupId, trainings }) {
    return axios
      .put(`/groups/${groupId}/trainings/order`, { trainings }, {withCredentials: true})
      .then(() => {
        commit(SET_GROUP_TRAININGS, { groupId, trainings });
      })
      .catch((error) => {
        return Promise.reject(new Error(error));
      });
  },
  addGroup({ commit, dispatch },  groupName ) {
    return axios
      .post(`/groups/`, { group_name: groupName }, {withCredentials: true})
      .then((response) => {
        dispatch('getAllGroups');
        return response.data.id;
      })
      .catch((error) => {
        return Promise.reject(new Error(error));
      });
  },
  deleteGroup({ commit }, groupId ) {
    return axios
      .delete(`/groups/${groupId}` , {withCredentials: true})
      .then(() => {
        commit(DELETE_GROUP, groupId);
      })
      .catch((error) => {
        return Promise.reject(new Error(error));
      });
  },
};

const mutations = {
  [SET_ALL_GROUPS](state, groups) {
    state.all = groups;
  },
  [DELETE_GROUP](state, groupId) {
    Vue.delete(state.all, groupId);
  },
  [SET_GROUP_TRAININGS](state, { groupId, trainings }) {
    Vue.set(state.all[groupId], 'trainings', trainings);
  },
};

const groups = {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};

export default groups;

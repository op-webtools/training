import Vue from 'vue';
import axios from 'axios';

// Mutations
const SET_TRAINING = 'SET_TRAINING';
const DELETE_TRAINING = 'DELETE_TRAINING';

const state = {
  all: {},
};

const getters = {
  trainingById: (state) => (trainingId) => {
    return state.all[trainingId];
  },
};

const actions = {
  getTraining({ commit, state }, trainingId) {
    if (state.all[trainingId]) {
      return Promise.resolve(state.all[trainingId]);
    }
    return axios
      .get(`/trainings/${trainingId}`, { withCredentials: true })
      .then((response) => {
        const data = response.data;
        commit(SET_TRAINING, data);
        return data;
      })
      .catch((error) => {
        return Promise.reject(new Error(error));
      });
  },
  addTraining({ commit }, training) {
    return axios
      .post(`/trainings/`,
        {
          withCredentials: true,
          courses: training.courses,
          description: training.description,
          groups: training.groups,
          responsible: training.responsible,
          title: training.title,
        })
      .then((response) => {
        const data = response.data;
        commit(SET_TRAINING, data);
        return data;
      })
      .catch((error) => {
        return Promise.reject(new Error(error));
      });
  },
  editTraining({ commit }, { trainingId, training}) {
    return axios
      .put(`/trainings/${trainingId}`,
        {
          withCredentials: true,
          trainingId,
          courses: training.courses,
          description: training.description,
          groups: training.groups,
          responsible: training.responsible,
          title: training.title,
        })
      .then(() => {
        training.id = trainingId;
        commit(SET_TRAINING, training);
        return training;
      })
      .catch((error) => {
        return Promise.reject(new Error(error));
      });
  },
  searchTrainings({ commit }, {admin, text, groupId}) {
    return axios
      .get(`/trainings/search`, {
        withCredentials: true,
        params: {
          admin,
          text,
          groupId,
        },
      })
      .then((response) => {
        const data = response.data;
        data.forEach((training) => {
          commit(SET_TRAINING, training);
        });
        return data;
      })
      .catch((error) => {
        return Promise.reject(new Error(error));
      });
  },
  userClassroomTrainingsOrder({ commit, dispatch }, {classroomId, userId, trainings}) {
    return axios
      .put(`/classrooms/${classroomId}/users/${userId}/trainings/order`, {
        withCredentials: true,
        trainings,
      })
      .then((response) => {
        const data = response.data;
        // maybe can just update course responsible locally
        dispatch('courses/getUserCourses', { userId, refresh: true }, { root: true });
        return data;
      })
      .catch((error) => {
        return Promise.reject(new Error(error));
      });
  },
  deleteTraining({ state, commit, dispatch }, { trainingId }) {
    return axios
      .delete(`/trainings/${trainingId}`, {
        withCredentials: true,
      })
      .then((response) => {
        const courses = state.all[trainingId].courses.map((c) => c.edms);
        courses.forEach((c) => {
          dispatch('courses/decreaseCourseTrainingCount', c, { root: true });
        });
        commit('DELETE_TRAINING', trainingId);
      })
      .catch((error) => {
        return Promise.reject(new Error(error));
      });
  },
};

const mutations = {
  [SET_TRAINING](state, training) {
    Vue.set(state.all, training.id, training);
  },
  [DELETE_TRAINING](state, trainingId) {
    Vue.delete(state.all, trainingId);
  },
};

const trainings = {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};

export default trainings;

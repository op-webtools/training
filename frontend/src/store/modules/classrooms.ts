import Vue from 'vue';
import axios from 'axios';
import toastr from 'toastr';

// Mutations
const SET_CLASSROOM = 'SET_CLASSROOM';
const SET_ALL_CLASSROOMS = 'SET_ALL_CLASSROOMS';
const SET_VIEW_MODE = 'SET_VIEW_MODE';
const ADD_CLASSROOM_USER = 'ADD_CLASSROOM_USER';
const DELETE_CLASSROOM_USER = 'DELETE_CLASSROOM_USER';

export const state = {
  all: {},
  viewMode: 'list',
};

const getters = {};

const actions = {
  getAllClassrooms({ commit }) {
    return axios
      .get(`/classrooms/`, {
        withCredentials: true,
      })
      .then((response) => {
        const res = response.data.reduce((obj, item) => {
          obj[item.id] = item;
          return obj;
        }, {});
        commit(SET_ALL_CLASSROOMS, res);
      })
      .catch((error) => {
        return Promise.reject(new Error(error));
      });
  },
  setViewMode({ commit }, mode) {
    commit(SET_VIEW_MODE, mode);
  },
  getClassroom({ commit, state }, classroomId) {
    if (state.all[classroomId]) {
      return Promise.resolve(state.all[classroomId]);
    }
    return axios
      .get(`/classrooms/${classroomId}`, {
        withCredentials: true,
      })
      .then((response) => {
        commit(SET_CLASSROOM, response.data);
        return response.data;
      })
      .catch((error) => {
        return Promise.reject(new Error(error));
      });
  },
  addClassroomUser({ commit, state }, {classroomId, userId}) {
    return axios
      .post(`/classrooms/${classroomId}/users/${userId}`, {
        withCredentials: true,
      })
      .then((response) => {
        commit(ADD_CLASSROOM_USER, { classroomId, userId });
        toastr.success(`${userId} added`);
        return response.data;
      })
      .catch((error) => {
        toastr.error(`${userId} could not be added`);
        return Promise.reject(new Error(error));
      });
  },
  deleteClassroomUser({ commit, state }, {classroomId, userId}) {
    return axios
      .delete(`/classrooms/${classroomId}/users/${userId}`, {
        withCredentials: true,
      })
      .then((response) => {
        commit(DELETE_CLASSROOM_USER, { classroomId, userId });
        return response.data;
      })
      .catch((error) => {
        return Promise.reject(new Error(error));
      });
  },
};

const mutations = {
  [SET_ALL_CLASSROOMS](state, classrooms) {
    state.all = classrooms;
  },
  [SET_CLASSROOM](state, classroom) {
    Vue.set(state.all, classroom.id, classroom);
  },
  [SET_VIEW_MODE](state, mode) {
    state.viewMode = mode;
  },
  [DELETE_CLASSROOM_USER](state, { classroomId, userId }) {
    const usersList = state.all[classroomId].users.filter((a) => a !== userId);
    Vue.set(state.all[classroomId], 'users', usersList);
  },
  [ADD_CLASSROOM_USER](state, { classroomId, userId }) {
    const usersList = [...state.all[classroomId].users, userId];
    Vue.set(state.all[classroomId], 'users', usersList);
  },
};

const classrooms = {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};

export default classrooms;

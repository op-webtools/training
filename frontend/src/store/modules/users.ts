import Vue from 'vue';

import axios from 'axios';

// Mutations
const SET_USER_INFO = 'SET_USER_INFO';
const SET_CURRENT_USER = 'SET_CURRENT_USER';
const SET_USER_CLASSROOM = 'SET_USER_CLASSROOM';

const state = {
  all: {},
  current: null,
};

const getters = {};

const actions = {
  getCurrentUser({ commit }) {
    return axios
      .get(`/users/current`, { withCredentials: true })
      .then((response) => {
        const data = response.data;
        if (data && data.user_id) {
          commit(SET_USER_INFO, data);
          commit(SET_CURRENT_USER, data.user_id);
        }
        return data.user_id;
      })
      .catch((error) => {
        return Promise.reject(new Error(error));
      });
  },
  getUserInfo({ commit, state }, userId) {
    if (state.all[userId]) {
      return Promise.resolve(state.all[userId]);
    }
    return axios
      .get(`/users/${userId}`, { withCredentials: true })
      .then((response) => {
        const data = response.data;
        commit(SET_USER_INFO, data);
        return data;
      })
      .catch((error) => {
        return Promise.reject(new Error(error));
      });
  },
  getUserClassrooms({ commit, dispatch }, userId) {
    return axios
      .get(`/users/${userId}/classrooms`, { withCredentials: true })
      .then((response) => {
        const classrooms = response.data;
        commit(SET_USER_CLASSROOM, { userId, classrooms });
        classrooms.forEach((classroomId) => {
          dispatch('classrooms/getClassroom', classroomId, {
            root: true,
          });
        });


        return classrooms;
      })
      .catch((error) => {
        return Promise.reject(new Error(error));
      });
  },
};

const mutations = {
  [SET_USER_INFO](state, user) {
    Vue.set(state.all, user.user_id, { ...state.all[user.user_id], ...user });
  },
  [SET_USER_CLASSROOM](state, { userId, classrooms }) {
    Vue.set(state.all, userId, { ...state.all[userId], ...{ classrooms } });
  },
  [SET_CURRENT_USER](state, user) {
    state.current = user;
  },
};

const users = {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};

export default users;

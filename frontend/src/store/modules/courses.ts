import Vue from 'vue';
import axios from 'axios';

// Mutations
const SET_COURSE = 'SET_COURSE';
const RESET_COURSES = 'RESET_COURSES';
const DELETE_COURSE = 'DELETE_COURSE';
const SET_USER_COURSES = 'SET_USER_COURSES';
const UPDATE_TRAINING_COUNT = 'UPDATE_TRAINING_COUNT';

const state = {
  all: {},
  users: {},
};

const getters = {
  unassigned(state) {
    return Object.values(state.all).filter(
      (course: any) => course['trainings'] === 0,
    );
  },
  userCoursesByClassroom: (state) => (userId, classroomId) => {
    if (!state.users[userId]) {
      return [];
    }
    return state.users[userId].filter((c) => c.classroom_id === classroomId);
  },
  userCoursesByTraining: (state, getters, rootState, rootGetters) => (userId, trainingId, classroomId) => {
    if (!state.users[userId]) {
      return [];
    }
    const training = rootGetters['trainings/trainingById'](trainingId);
    let courses = state.users[userId].filter(
      (c) => (c.training_id === trainingId && c.classroom_id === classroomId),
    );
    if (training && training.courses) {
      courses = courses.sort(
        (a, b) => {
          const aPos = training.courses.find((e) => e.edms === a.edmsId);
          const bPos = training.courses.find((e) => e.edms === b.edmsId);
          if (aPos && bPos) {
            return aPos.position - bPos.position;
          } else {
            return 0;
          }
        },
      );
    }
    return courses;
  },
};

const actions = {
  getCourseInfo({ commit, state }, courseId) {
    if (state.all[courseId] && state.all[courseId].title) {
      return Promise.resolve(state.all[courseId]);
    }
    return axios
      .get(`/edms/${courseId}`, { withCredentials: true })
      .then((response) => {
        const data = response.data;
        if (data) {
          commit(SET_COURSE, data);
        }
        return data;
      })
      .catch((error) => {
        return Promise.reject(new Error(error));
      });
  },
  getEDMSFolderCourses({ commit, state }, EDMSFolder) {
    return axios
      .get(`/edms/folder/${EDMSFolder}`, { withCredentials: true })
      .then((response) => {
        const data = response.data;
        let res = [];
        const EDMSIds = Object.keys(state.all);
        if (data) {
          res = data.filter((c) => {
            return !EDMSIds.includes(c.edmsId.toString());
          });
        }
        return res;
      })
      .catch((error) => {
        return Promise.reject(new Error(error));
      });
  },
  getAllCourses({ commit, dispatch }) {
    return axios
      .get(`/courses`, { withCredentials: true })
      .then((response) => {
        const data = response.data;
        if (data) {
          data.forEach((course) => {
            course.edmsId = course.edmsid;
            // Preload only unassigned courses
            if (!course.trainings) {
              dispatch('getCourseInfo', course.edmsId);
            }
            commit(SET_COURSE, course);
          });
        }
        return data;
      })
      .catch((error) => {
        return Promise.reject(new Error(error));
      });
  },
  getAllCoursesInfo({ commit, state, dispatch }) {
    const promises: any = [];
    Object.values(state.all).forEach((course: any) => {
      if (!course.title) {
        promises.push(dispatch('getCourseInfo', course.edmsId));
      }
    });
    return Promise.all(promises);
  },
  getUserCourses({ commit, state }, { userId, refresh }) {
    if (state.users[userId] && refresh !== true) {
      return Promise.resolve(state.users[userId]);
    }
    return axios
      .get(`/users/${userId}/courses`, { withCredentials: true })
      .then((response) => {
        const data = response.data;
        if (data) {
          data.forEach((course) => {
            course.edmsId = course.course_edms;
            commit(SET_COURSE, course);
          });
          commit(SET_USER_COURSES, { data, userId });
        }
        return data;
      })
      .catch((error) => {
        return Promise.reject(new Error(error));
      });
  },
  addUserTrainingCourses(
    { commit, dispatch },
    { classroomId, userId, trainingId },
  ) {
    return axios
      .post(
        `/classrooms/${classroomId}/users/${userId}/trainings/${trainingId}/`,
        {
          withCredentials: true,
        },
      )
      .then((response) => {
        dispatch('getUserCourses', { userId, refresh: true });
      })
      .catch((error) => {
        return Promise.reject(new Error(error));
      });
  },
  deleteUserTrainingCourses(
    { commit, dispatch },
    { classroomId, userId, trainingId },
  ) {
    return axios
      .delete(
        `/classrooms/${classroomId}/users/${userId}/trainings/${trainingId}/`,
        {
          withCredentials: true,
        },
      )
      .then((response) => {
        dispatch('getUserCourses', { userId, refresh: true });
      })
      .catch((error) => {
        return Promise.reject(new Error(error));
      });
  },
  deleteCourse({ commit }, courseId) {
    return axios
      .delete(`/courses/${courseId}/`, {
        withCredentials: true,
      })
      .then((response) => {
        commit(DELETE_COURSE, courseId);
      })
      .catch((error) => {
        return Promise.reject(new Error(error));
      });
  },
  addUserCourse(
    { commit, dispatch },
    { classroomId, userId, trainingId, edmsId },
  ) {
    return axios
      .post(
        `/classrooms/${classroomId}/users/${userId}/trainings/${trainingId}/courses/${edmsId}`,
        {
          withCredentials: true,
        },
      )
      .then((response) => {
        dispatch('getUserCourses', { userId, refresh: true });
      })
      .catch((error) => {
        return Promise.reject(new Error(error));
      });
  },
  downloadEDMSCourse({ commit, dispatch }, edmsId) {
    return axios
      .post(`/courses/`, {
        withCredentials: true,
        edmsId,
      })
      .then((response) => {
        const data = {
          edmsId,
          trainings: 0, // New courses should not be in any training!
        };
        commit(SET_COURSE, data);
        dispatch('getCourseInfo', edmsId);
      })
      .catch((error) => {
        return Promise.reject(new Error(error));
      });
  },
  removeUserCourse(
    { commit, dispatch },
    { classroomId, userId, trainingId, edmsId },
  ) {
    return axios
      .delete(
        `/classrooms/${classroomId}/users/${userId}/trainings/${trainingId}/courses/${edmsId}`,
        {
          withCredentials: true,
        },
      )
      .then((response) => {
        dispatch('getUserCourses', { userId, refresh: true });
      })
      .catch((error) => {
        return Promise.reject(new Error(error));
      });
  },
  setUserCourseDeadline(
    { commit, dispatch },
    { classroomId, userId, trainingId, edmsId, deadline },
  ) {
    return axios
      .put(
        `/classrooms/${classroomId}/users/${userId}/trainings/${trainingId}/courses/${edmsId}/deadline`,
        {
          withCredentials: true,
          deadline,
        },
      )
      .then((response) => {
        // maybe can just update course deadline locally
        dispatch('getUserCourses', { userId, refresh: true });
      })
      .catch((error) => {
        return Promise.reject(new Error(error));
      });
  },
  setUserCourseValidators(
    { commit, dispatch },
    { classroomId, userId, trainingId, edmsId, responsible },
  ) {
    return axios
      .put(
        `/classrooms/${classroomId}/users/${userId}/trainings/${trainingId}/courses/${edmsId}/responsible`,
        {
          withCredentials: true,
          responsible,
        },
      )
      .then((response) => {
        // maybe can just update course responsible locally
        dispatch('getUserCourses', { userId, refresh: true });
      })
      .catch((error) => {
        return Promise.reject(new Error(error));
      });
  },
  startUserCourse({ commit, dispatch }, { userId, edmsId }) {
    return axios
      .put(`/users/${userId}/courses/${edmsId}/start`, {
        withCredentials: true,
      })
      .then((response) => {
        // maybe can just update course responsible locally
        dispatch('getUserCourses', { userId, refresh: true });
      })
      .catch((error) => {
        return Promise.reject(new Error(error));
      });
  },
  completeUserCourse({ commit, dispatch }, { userId, edmsId }) {
    return axios
      .put(`/users/${userId}/courses/${edmsId}/complete`, {
        withCredentials: true,
      })
      .then((response) => {
        // maybe can just update course responsible locally
        dispatch('getUserCourses', { userId, refresh: true });
      })
      .catch((error) => {
        return Promise.reject(new Error(error));
      });
  },
  validateUserCourse({ commit, dispatch }, { userId, edmsId }) {
    return axios
      .put(`/users/${userId}/courses/${edmsId}/validate`, {
        withCredentials: true,
      })
      .then((response) => {
        // maybe can just update course responsible locally
        dispatch('getUserCourses', { userId, refresh: true });
      })
      .catch((error) => {
        return Promise.reject(new Error(error));
      });
  },
  increaseCourseTrainingCount({ commit }, courseId) {
    commit(UPDATE_TRAINING_COUNT, { courseId, upOrDown: 'increase' });
  },
  decreaseCourseTrainingCount({ commit }, courseId) {
    commit(UPDATE_TRAINING_COUNT, { courseId, upOrDown: 'decrease' });
  },
  resetCourses({ commit }) {
    commit(RESET_COURSES);
  },
};

const mutations = {
  [SET_COURSE](state, course) {
    Vue.set(state.all, course.edmsId, {...state.all[course.edmsId], ...course});
  },
  [DELETE_COURSE](state, courseId) {
    Vue.delete(state.all, courseId);
  },
  [SET_USER_COURSES](state, {data, userId}) {
    Vue.set(state.users, userId, data);
  },
  [RESET_COURSES](state) {
    Object.assign(state, {
      all: {},
      users: {},
    });
  },
  [UPDATE_TRAINING_COUNT](state, {courseId, upOrDown}) {
    if (upOrDown === 'increase') {
      state.all[courseId].trainings += 1;
    } else if (upOrDown === 'decrease') {
      state.all[courseId].trainings -= 1;
    }
  },
};

const courses = {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};

export default courses;

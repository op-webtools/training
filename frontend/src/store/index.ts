import Vue from 'vue'
import Vuex from 'vuex'
import users from './modules/users';
import courses from './modules/courses';
import groups from './modules/groups';
import trainings from './modules/trainings';
import classrooms from './modules/classrooms';

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
  },
  getters: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    users,
    courses,
    groups,
    trainings,
    classrooms,
  }
})

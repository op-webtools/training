const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  publicPath: '/training/',
  transpileDependencies: [
    'vuetify'
  ]
})

<?php

class RequestHandler {

    private $baseUrl;
    private $method;
    private $data;
    private $handler;
    private $found = false;
    private $routeArgs = array();
    private $allRoutes = array();


    public function __construct($baseUrl) {
        $this->baseUrl = trim($baseUrl, '/');
        $this->_setMethod();
    }


    public function getEndpoints() {
        return $this->allRoutes;
    }


    private function _setMethod() {
        $this->method = $_SERVER['REQUEST_METHOD'];
        if ($this->method === 'POST' && array_key_exists('HTTP_X_HTTP_METHOD', $_SERVER)) {
            if ($_SERVER['HTTP_X_HTTP_METHOD'] === 'DELETE') {
                $this->method = 'DELETE';
            } else if ($_SERVER['HTTP_X_HTTP_METHOD'] === 'PUT') {
                $this->method = 'PUT';
            }
        }
        if (!$this->_isValidMethod()) Http::send_method_not_allowed();
    }


    private function _isValidMethod() {
        if (in_array($this->method, array('GET', 'POST', 'PUT', 'DELETE'))) {
            return true;
        }
        return false;
    }


    public function get($routeShape, $callback) {
        $this->_setHandler('GET', $routeShape, $callback);
    }

    public function post($routeShape, $callback) {
        $this->_setHandler('POST', $routeShape, $callback);
    }

    public function put($routeShape, $callback) {
        $this->_setHandler('PUT', $routeShape, $callback);
    }

    public function delete($routeShape, $callback) {
        $this->_setHandler('DELETE', $routeShape, $callback);
    }

    public function patch($routeShape, $callback) {
        $this->_setHandler('PATCH', $routeShape, $callback);
    }


    private function _setHandler($method, $routeShape, $callback) {
        $this->allRoutes[] = array('method' => $method, 'name' => $routeShape);

        if (!$this->found && $this->method === $method && $this->_isCurrentRoute($routeShape)) {
            $this->handler = $callback;
            $this->_setData($method);
            $this->found = true; // Avoids more complex operations on subsequent comparisons
        }
    }


    private function _setData($method) {
        switch ($method) {
            case 'GET':
                $this->data = $_GET;
                break;
            case 'POST': // $this->data = $this->_cleanInput( array_merge($_POST, $_FILES) );
            case 'PUT':  // $putData = fopen("php://input", "r");
            case 'PATCH':
            case 'DELETE':
                // parse_str(file_get_contents("php://input", true), $input);
                $input = json_decode(file_get_contents("php://input", true), true);
                $this->data = $input;
                break;

            default: Http::send_method_not_allowed();
        }
    }


    public function handle($opts = null) {
        if (! isset($this->handler)) {
            if ($opts['die'] === false) {
                return;
            } else {
                Http::send_error('The URL you have requested is not valid',
                    'Check the route handlers in your code');
            }
        }

        if (count($this->data) === 0) {
            $this->data = null;
        }
        foreach ($this->routeArgs as &$arg) {
            $arg = urldecode($arg);
        }
        call_user_func_array($this->handler, array_merge($this->routeArgs, array($this->data)));
    }


    private function _isCurrentRoute($routeShape) {
        $routeShape = trim($routeShape, '/');
        $realRoute  = $_SERVER['REQUEST_URI']; // Important to be unencoded, otherwise we could find more slashes than expected

        // Get rid of GET parameters
        $questionMarkPos = strpos($realRoute, '?');
        if ($questionMarkPos !== false) {
            $realRoute = substr($realRoute, 0, $questionMarkPos);
        }
        $realRoute = trim($realRoute, '/');

        // Get the URL parameters (not the ones sent via GET, but those that make REST work nicely)
        $realUri = '';
        $pos = strpos($realRoute, $this->baseUrl);
        if ($pos === false) return false;

        $realUri = substr($realRoute, $pos + strlen($this->baseUrl) + 1);
        if ($realUri === false) $realUri = '';

        $routeChunks = explode('/', $realUri);
        $routeShapeChunks = explode('/', $routeShape);

        if (count($routeChunks) !== count($routeShapeChunks)) return false;

        $params = array();
        for ($i = 0; $i < count($routeChunks); ++$i) {
            // Variable on the URL
            if (preg_match('/:(int|float|string)/', $routeShapeChunks[$i]) === 1) {
                $type = substr($routeShapeChunks[$i], 1);

                if ( ($type === 'int' && !$this->_isStringInt($routeChunks[$i])) ||
                    ($type === 'float' && !$this->_isStringFloat($routeChunks[$i])) ) {
                    return false;
                }
                $param = $routeChunks[$i];
                settype($param, $type);
                $params[] = $param;
            }
            // Fixed part of the URL
            else if ($routeChunks[$i] !== $routeShapeChunks[$i]) return false;
        }
        $this->routeArgs = $params;
        return true;
    }


    private function _isStringInt($str) {
        if ($str[0] === '+' || $str[0] === '-') {
            $str = substr($str, 1);
        }
        return ctype_digit($str);
    }


    private function _isStringFloat($str) {
        $parts = explode('.', $str);
        if (count($parts) === 2) {
            if ($parts[0][0] === '+' || $parts[0][0] === '-') {
                $parts[0] = substr($parts[0], 1);
            }
            return ctype_digit($parts[0]) && ctype_digit($parts[1]);
        }
        return false;
    }

}
<?php

class DB {
    private static $config;
    private static $dbh;

    public static function config($config)
    {
        self::$config = $config;
    }

    public static function connect()
    {
        if(!self::$dbh){
            try {
                // Open persistent DB connection.
                self::$dbh = new PDO(
                    self::$config["pdo"],
                    self::$config["usr"],
                    self::$config["pwd"],
                    array(
                        PDO::ATTR_PERSISTENT => true
                    )
                );
            } catch (PDOException $e) {
                Http::send_error($e->getMessage());
            }
        }
        return self::$dbh;
    }
}
<?PHP

function dbConnection($db){

    switch ($db) {
        case "camms_pub":
            $connection = array("usr" => "camms_pub","pwd" => getenv("DB_PWD"), "pdo"=> "oci:dbname=EDMSDB");
            break;
        case "beoptir":
            $connection = array("usr" => "beoptir","pwd" => getenv("DB_PWD"), "pdo"=> "oci:dbname=EDMSDB");
            break;
        case "beop_im":
            $connection = array("usr" => "beop_im","pwd" => getenv("DB_PWD"), "pdo"=> "oci:dbname=EDMSDB");
            break;
        case "logbook_ti_dev":
            $connection = array("usr" => "logbook_ti_dev","pwd" => getenv("DB_PWD"),"pdo"=> "oci:dbname=DEVDB19");
            break;
        case "opwebtools":
            $connection = array("usr" => "opwebtools","pwd" => getenv("DB_PWD"), "pdo"=> "oci:dbname=cerndb1");
            break;
        case "abop_planning":
            $connection = array("usr" => "abop_planning","pwd" => getenv("DB_PWD"),"pdo"=> "oci:dbname=EDMSDB");
            break;
    }
    return $connection;
}
/*
USAGE
require_once "connection.php";
$connection = dbConnection("logbook_ti_dev");
*/
?>
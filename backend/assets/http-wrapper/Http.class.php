<?php

// Ensure backwards compatibility with PHP < 5.4.0, where http_response_code is not defined
_define_http_response_code();
_define_pretty_json();

define('ALLOW_CORS', true); // TO-DO: Move outside


class Http {

    public static function send_error($message, $extraInfo = '', $code = 400) {
        Http::send_cors_headers();
        http_response_code($code);
        header('Content-Type: application/json');
        echo json_encode(array('user_message' => $message,
            'dev_message' => $extraInfo), JSON_PRETTY_PRINT);
        exit;
    }

    public static function send_cors_headers() {
        if (ALLOW_CORS === true) {

            $incomingOrigin = array_key_exists('HTTP_ORIGIN', $_SERVER) ? $_SERVER['HTTP_ORIGIN'] : null;

            if ($incomingOrigin !== null )
            {
                header("Access-Control-Allow-Origin: ".$_SERVER['HTTP_ORIGIN']);
            } else {
                header("Access-Control-Allow-Origin: *");
            }
            header("Access-Control-Allow-Credentials: true");
            // header("Access-Control-Allow-Methods: OPTIONS, DELETE, PUT");
            header("Access-Control-Expose-Headers:");
            header("Access-Control-Max-Age:1728000");
            header("Access-Control-Allow-Methods: *");
            header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Authorization");
        }
    }

    public static function send_json($arr, $code = 200) {
        Http::send_cors_headers();
        http_response_code($code);
        header('Content-Type: application/json');
        echo json_encode($arr, JSON_NUMERIC_CHECK | JSON_PRETTY_PRINT);
        exit;
    }

    public static function send_download_json($arr, $code = 200, $title) {
        Http::send_cors_headers();
        http_response_code($code);
        if (!$title) {
            $now = date("dMY");
            $title = "download_$now";
        }
        header("Content-Disposition: attachment; filename=$title.json");
        header('Content-Type: application/json');
        echo json_encode($arr, JSON_NUMERIC_CHECK | JSON_PRETTY_PRINT);
        exit;
    }

    public static function send_csv($str, $code = 200) {
        Http::send_cors_headers();
        http_response_code($code);
        header('Content-Type: text/csv');
        echo $str;
        exit;
    }

    public static function send_download_csv(array &$array,  $code = 200, $title) {
        Http::send_cors_headers();
        http_response_code($code);
        if (!$title) {
            $now = date("dMY");
            $title = "download_$now";
        }
        header("Content-Disposition: attachment; filename=$title.csv");
        header('Content-Type: text/csv');

        ob_start();
        $df = fopen("php://output",'w');
        foreach ($array as $row) {
            fputcsv($df, $row);
        }
        fclose($df);
        echo ob_get_clean();
        exit;
    }

    public static function send_ok($arr = null) {
        if (isset($arr)) {
            Http::send_json($arr, 200);
        }
        Http::send_cors_headers();
        http_response_code(200);
        exit;
    }

    public static function send_created($arr = null) {
        if (isset($arr)) {
            Http::send_json($arr, 201);
        }
        Http::send_cors_headers();
        http_response_code(201);
        exit;
    }

    public static function send_deleted($arr = null) {
        if (isset($arr)) {
            Http::send_json($arr, 204);
        }
        Http::send_cors_headers();
        http_response_code(204);
        exit;
    }

    public static function send_bad_request() {
        Http::send_cors_headers();
        http_response_code(400);
        exit;
    }

    public static function send_not_found() {
        Http::send_cors_headers();
        http_response_code(404);
        exit;
    }

    public static function send_method_not_allowed() {
        Http::send_cors_headers();
        http_response_code(405);
        exit;
    }

    public static function send_unsupported_media_type() {
        Http::send_cors_headers();
        http_response_code(415);
        exit;
    }

    public static function send_internal_server_error() {
        Http::send_cors_headers();
        http_response_code(500);
        exit;
    }

}


function _define_pretty_json() {
    if (!defined('JSON_PRETTY_PRINT')) {
        define('JSON_PRETTY_PRINT', 0);
    }
}

function _define_http_response_code() {
    if (!function_exists('http_response_code')) {
        function http_response_code($code) {
            switch ($code) {
                case 100: $text = 'Continue'; break;
                case 101: $text = 'Switching Protocols'; break;
                case 200: $text = 'OK'; break;
                case 201: $text = 'Created'; break;
                case 202: $text = 'Accepted'; break;
                case 203: $text = 'Non-Authoritative Information'; break;
                case 204: $text = 'No Content'; break;
                case 205: $text = 'Reset Content'; break;
                case 206: $text = 'Partial Content'; break;
                case 300: $text = 'Multiple Choices'; break;
                case 301: $text = 'Moved Permanently'; break;
                case 302: $text = 'Moved Temporarily'; break;
                case 303: $text = 'See Other'; break;
                case 304: $text = 'Not Modified'; break;
                case 305: $text = 'Use Proxy'; break;
                case 400: $text = 'Bad Request'; break;
                case 401: $text = 'Unauthorized'; break;
                case 402: $text = 'Payment Required'; break;
                case 403: $text = 'Forbidden'; break;
                case 404: $text = 'Not Found'; break;
                case 405: $text = 'Method Not Allowed'; break;
                case 406: $text = 'Not Acceptable'; break;
                case 407: $text = 'Proxy Authentication Required'; break;
                case 408: $text = 'Request Time-out'; break;
                case 409: $text = 'Conflict'; break;
                case 410: $text = 'Gone'; break;
                case 411: $text = 'Length Required'; break;
                case 412: $text = 'Precondition Failed'; break;
                case 413: $text = 'Request Entity Too Large'; break;
                case 414: $text = 'Request-URI Too Large'; break;
                case 415: $text = 'Unsupported Media Type'; break;
                case 500: $text = 'Internal Server Error'; break;
                case 501: $text = 'Not Implemented'; break;
                case 502: $text = 'Bad Gateway'; break;
                case 503: $text = 'Service Unavailable'; break;
                case 504: $text = 'Gateway Time-out'; break;
                case 505: $text = 'HTTP Version not supported'; break;
                default: exit('Unknown HTTP status code "' . htmlentities($code) . '"');
            }
            $protocol = isset($_SERVER['SERVER_PROTOCOL']) ? $_SERVER['SERVER_PROTOCOL']
                : 'HTTP/1.0';
            header($protocol . ' ' . $code . ' ' . $text);
        }
    }
}
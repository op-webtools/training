<?php

/**
 * @SWG\Post(
 *     path="/email",
 *     summary="Send email.",
 *     tags={"All", "Email"},
 *     operationId="sendEmail",
 *     produces={
 *         "application/json"
 *     },
 *     @SWG\Parameter(
 *         in="body",
 *         name="body",
 *         required=true,
 *         @SWG\Schema(
 *             type="object",
 *             @SWG\Property(property="to", type="string"),
 *             @SWG\Property(property="title", type="string"),
 *             @SWG\Property(property="message", type="string")
 *         )
 *     ),
 *     @SWG\Response(
 *         response=201,
 *         description="Email sent"
 *     )
 * )
 */
$route->post('/email', function ($params) {
  Http::send_ok(Utils::sendEmail($params));
});

?>

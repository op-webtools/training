<?php

// ROUTES --- --- ---

/**
 * @SWG\Get(
 *     path="/trainings",
 *     summary="Get list of trainings",
 *     tags={"All", "Trainings"},
 *     operationId="getAllTrainings",
 *     produces={
 *         "application/json"
 *     },
 *     @SWG\Response(
 *         response=200,
 *         description="List of trainings.",
 *         @SWG\Schema(
 *             type="array",
 *             @SWG\Items(
 *                 type="object",
 *                 @SWG\Property(property="ID", type="integer"),
 *                 @SWG\Property(property="TITLE", type="string"),
 *                 @SWG\Property(property="DESCRIPTION", type="string"),
 *                 @SWG\Property(property="RESPONSIBLE", type="string"),
 *                 @SWG\Property(property="GROUP_NAME", type="string"),
 *                 @SWG\Property(property="CREATED", type="string"),
 *                 @SWG\Property(property="UPDATED", type="string")
 *             )
 *         )
 *     )
 * )
 */
$route->get(
  '/trainings',
  function() {
    Http::send_json(getAllTrainings());
  }
);

/**
 * @SWG\Get(
 *     path="/trainings/search",
 *     summary="Search trainings",
 *     tags={"All", "Trainings"},
 *     operationId="searchAllTrainings",
 *     produces={
 *         "application/json"
 *     },
 *     @SWG\Parameter(
 *         in="query",
 *         name="text",
 *         schema=@SWG\Schema(
 *            @SWG\Property(property="text", type="string"),
 *         )
 *     ),
 *     @SWG\Parameter(
 *         in="query",
 *         name="admin",
 *         schema=@SWG\Schema(
 *            @SWG\Property(property="admin", type="string"),
 *         )
 *     ),
 *     @SWG\Response(
 *         response=200,
 *         description="List of trainings.",
 *         @SWG\Schema(
 *             type="array",
 *             @SWG\Items(
 *                 type="object",
 *                 @SWG\Property(property="ID", type="integer"),
 *                 @SWG\Property(property="TITLE", type="string"),
 *                 @SWG\Property(property="DESCRIPTION", type="string"),
 *                 @SWG\Property(property="RESPONSIBLE", type="string"),
 *                 @SWG\Property(property="GROUP_NAME", type="string"),
 *                 @SWG\Property(property="CREATED", type="string"),
 *                 @SWG\Property(property="UPDATED", type="string")
 *             )
 *         )
 *     )
 * )
 */
$route->get(
  '/trainings/search',
  function($params) {
    Http::send_json(searchAllTrainings($params));
  }
);

/**
 * @SWG\Get(
 *     path="/trainings/{trainingId}",
 *     summary="Get training data",
 *     tags={"All", "Trainings"},
 *     operationId="getTraining",
 *     produces={
 *         "application/json"
 *     },
 *     @SWG\Parameter(
 *         in="path",
 *         name="trainingId",
 *         required=true,
 *         type="integer"
 *     ),
 *     @SWG\Response(
 *         response=200,
 *         description="training data.",
 *         @SWG\Schema(
 *            @SWG\Property(property="ID", type="integer"),
 *            @SWG\Property(property="TITLE", type="string"),
 *            @SWG\Property(property="DESCRIPTION", type="string"),
 *            @SWG\Property(property="RESPONSIBLE", type="string"),
 *            @SWG\Property(property="GROUP_NAME", type="string"),
 *            @SWG\Property(property="CREATED", type="string"),
 *            @SWG\Property(property="UPDATED", type="string"),
 *            @SWG\Property(property="POSITION", type="integer"),
 *            @SWG\Property(property="COURSES_COUNT", type="integer")
 *         )
 *     )
 * )
 */
$route->get(
  '/trainings/:int',
  function($trainingId) {
    Http::send_json(getTraining($trainingId));
  }
);

/**
 * @SWG\Get(
 *     path="/trainings/{trainingId}/courses",
 *     summary="Get training courses",
 *     tags={"All", "Trainings", "Courses"},
 *     operationId="getTrainingCourses",
 *     produces={
 *         "application/json"
 *     },
 *     @SWG\Parameter(
 *         in="path",
 *         name="trainingId",
 *         required=true,
 *         type="integer"
 *     ),
 *     @SWG\Response(
 *         response=200,
 *         description="training courses.",
 *         @SWG\Schema(
 *             type="array",
 *             @SWG\Items(
 *                 type="object",
 *                 @SWG\Property(property="ID", type="integer"),
 *                 @SWG\Property(property="title", type="string"),
 *                 @SWG\Property(property="position", type="integer")
 *             )
 *         )
 *     )
 * )
 */
$route->get(
  '/trainings/:int/courses',
  function($trainingId) {
    Http::send_json(getTrainingCourses($trainingId));
  }
);

/**
 * @SWG\Post(
 *     path="/trainings",
 *     summary="Create new training",
 *     tags={"All", "Trainings"},
 *     operationId="createTraining",
 *     produces={
 *         "application/json"
 *     },
 *     @SWG\Parameter(
 *         in="body",
 *         name="body",
 *         required=true,
 *         schema=@SWG\Schema(
 *            @SWG\Property(property="TITLE", type="string"),
 *            @SWG\Property(property="DESCRIPTION", type="string"),
 *            @SWG\Property(property="RESPONSIBLE", type="string"),
 *            @SWG\Property(property="COURSES", type="array",
 *             @SWG\Items(
 *                 type="object",
 *                 @SWG\Property(property="ID", type="integer")
 *             )
 *            ),
 *            @SWG\Property(property="groups", type="array",
 *             @SWG\Items(
 *              @SWG\Parameter(type="integer"),
 *             )
 *            )
 *         )
 *     ),
 *     @SWG\Response(
 *         response=200,
 *         description="training has been created.",
 *         @SWG\Schema(
 *             @SWG\Property(property="ID", type="integer")
 *         )
 *     )
 * )
 */
$route->post(
  '/trainings',
  function($params) {
    if (Utils::isMainAdmin()) {
      createTraining($params);
    } else {
      Utils::sendPrivligesErr();
    }
  }
);

/**
 * @SWG\Post(
 *     path="/trainings/{trainingId}/courses",
 *     summary="Add training courses",
 *     tags={"All", "Trainings", "Courses"},
 *     operationId="addTrainingCourses",
 *     produces={
 *         "application/json"
 *     },
 *     @SWG\Parameter(
 *         in="path",
 *         name="trainingId",
 *         required=true,
 *         type="integer"
 *     ),
 *     @SWG\Parameter(
 *         in="body",
 *         name="body",
 *         required=true,
 *         @SWG\Schema(
 *             type="array",
 *             @SWG\Items(
 *                 type="object",
 *                 @SWG\Property(property="ID", type="integer")
 *             )
 *         )
 *     ),
 *     @SWG\Response(
 *         response=200,
 *         description="courses have been added to training.",
 *     )
 * )
 */
$route->post(
  '/trainings/:int/courses',
  function($trainingId, $params) {
    if (isAdmin()) {
      addTrainingCourses($trainingId, $params);
    } else {
      Utils::sendPrivligesErr();
    }
  }
);

/**
 * @SWG\Put(
 *     path="/trainings/{trainingId}",
 *     summary="Update training data",
 *     tags={"All", "Trainings"},
 *     operationId="updateTraining",
 *     produces={
 *         "application/json"
 *     },
 *     @SWG\Parameter(
 *         in="path",
 *         name="trainingId",
 *         required=true,
 *         type="integer"
 *     ),
 *     @SWG\Parameter(
 *         in="body",
 *         name="body",
 *         required=true,
 *         schema=@SWG\Schema(
 *            @SWG\Property(property="TITLE", type="string"),
 *            @SWG\Property(property="DESCRIPTION", type="string"),
 *            @SWG\Property(property="RESPONSIBLE", type="string"),
 *         )
 *     ),
 *     @SWG\Response(
 *         response=200,
 *         description="training data has been updated."
 *     )
 * )
 */
$route->put(
  '/trainings/:int',
  function($trainingId, $params) {
    if (Utils::isTrainingAdmin($trainingId)) {
      updateTraining($trainingId, $params);
    } else {
      Utils::sendPrivligesErr();
    }
  }
);

/**
 * @SWG\Delete(
 *     path="/trainings/{trainingId}",
 *     summary="Delete training",
 *     tags={"All", "Trainings"},
 *     operationId="deleteTraining",
 *     produces={
 *         "application/json"
 *     },
 *     @SWG\Parameter(
 *         in="path",
 *         name="trainingId",
 *         required=true,
 *         type="integer"
 *     ),
 *     @SWG\Response(
 *         response=204,
 *         description="Successfully deleted training."
 *     )
 * )
 */
$route->delete(
  '/trainings/:int',
  function($trainingId) {
    if (Utils::isTrainingAdmin($trainingId)) {
      deleteTraining($trainingId);
    } else {
      Utils::sendPrivligesErr();
    }
  }
);

/**
 * @SWG\Delete(
 *     path="/trainings/{trainingId}/courses/{courseId}",
 *     summary="Delete training course",
 *     tags={"All", "Trainings", "Courses"},
 *     operationId="deleteTrainingCourses",
 *     produces={
 *         "application/json"
 *     },
 *     @SWG\Parameter(
 *         in="path",
 *         name="trainingId",
 *         required=true,
 *         type="integer"
 *     ),
 *     @SWG\Parameter(
 *         in="path",
 *         name="courseId",
 *         required=true,
 *         type="integer"
 *     ),
 *     @SWG\Response(
 *         response=204,
 *         description="Successfully deleted  courses from training."
 *     )
 * )
 */

$route->delete('/trainings/:int/courses/:int',
  function ($trainingId, $courseId) {
    if (isAdmin()) {
      deleteTrainingCourses($trainingId, $courseId);
    } else {
      Utils::sendPrivligesErr();
    }
  }
);

/**
 * @SWG\Delete(
 *     path="/trainings/{trainingId}/courses",
 *     summary="Delete all training courses",
 *     tags={"All", "Trainings", "Courses"},
 *     operationId="deleteTrainingCourses",
 *     produces={
 *         "application/json"
 *     },
 *     @SWG\Parameter(
 *         in="path",
 *         name="trainingId",
 *         required=true,
 *         type="integer"
 *     ),
 *     @SWG\Response(
 *         response=204,
 *         description="Successfully deleted  courses from training."
 *     )
 * )
 */
$route->delete('/trainings/:int/courses',
  function ($trainingId) {
    if (isAdmin()) {
      deleteTrainingCourses($trainingId);
    } else {
      Utils::sendPrivligesErr();
    }
  }
);

function getTrainingCourses($trainingId){

  $dbh = DB::connect();

  $q = $dbh->prepare("
    SELECT  course_edms as edms, position
    FROM tr_training_course tc
    LEFT JOIN tr_course c ON tc.course_edms = c.edms
    WHERE training_id = :trainingId
    ORDER BY position
  ");
  $q->bindParam(':trainingId', $trainingId, PDO::PARAM_INT, 10);

  if ($q->execute()) {
    return (Utils::array_change_key_case_recursive($q->fetchAll(PDO::FETCH_ASSOC)));
  } else {
    Http::send_error(
      'Error while getting training courses.',
      $q->errorInfo()
    );
  }

};

function getTrainingGroups($trainingId){

  $dbh = DB::connect();

  $q = $dbh->prepare("
    SELECT group_id
    FROM tr_training_group tg
    LEFT JOIN tr_group g ON tg.group_id = g.id
    WHERE training_id = :trainingId
  ");

  $q->bindParam(':trainingId', $trainingId, PDO::PARAM_INT, 10);

  if ($q->execute()) {
    return $q->fetchAll(PDO::FETCH_COLUMN, 0);
  } else {
    Http::send_error(
      'Error while getting training groups.',
      $q->errorInfo()
    );
  }

};

function addTrainingCourses($trainingId, $params){

  $dbh = DB::connect();
  $pos = 1;
  $query = 'INSERT ALL ';
  foreach ($params as $course) {
    $courseId = intval($course["ID"]);
    $query .= "INTO tr_training_course (training_id, course_id, position) VALUES (:trainingId, $courseId, $pos) ";
    $pos++;
  }
  $query .= 'SELECT 1 FROM DUAL';
  $q = $dbh->prepare($query);

  $q->bindParam(':trainingId', $trainingId, PDO::PARAM_INT, 10);

  if ($q->execute()) {
    Http::send_created("courses have been added to training.");
  } else {
    Http::send_error(
      'Error while adding courses to training.',
      $q->errorInfo()
    );
  }

};

function deleteTrainingCourses($trainingId, $courseId = null){

  $dbh = DB::connect();

  $query = "DELETE FROM tr_training_course WHERE training_id = :trainingId ";
  if($courseId){
    $query .= "AND course_id = :courseId";
  }

  $q = $dbh->prepare($query);

  if($courseId){
    $q->bindParam(':courseId', $courseId, PDO::PARAM_INT, 10);
  }
  $q->bindParam(':trainingId', $trainingId, PDO::PARAM_INT, 10);

  if ($q->execute()) {
    Http::send_deleted();
  } else {
    Http::send_error(
      'Error while removing courses from training.',
      $q->errorInfo()
    );
  }

};

function getAllTrainings(){

  $dbh = DB::connect();

  $q = $dbh->prepare("SELECT id, title, description, responsible, TO_CHAR(created, 'YYYY-MM-DD') AS created, TO_CHAR(updated, 'YYYY-MM-DD') AS updated FROM tr_training");

  if ($q->execute()) {
    $trainings = $q->fetchAll(PDO::FETCH_ASSOC);
    foreach ($trainings as &$training) {
      $training["groups"] = getTrainingGroups($training["ID"]);
      $training["courses"] = getTrainingCourses($training["ID"]);
    }
    return Utils::array_change_key_case_recursive($trainings);
  } else {
    Http::send_error(
      'Error while getting trainings.',
      $q->errorInfo()
    );
  }

}

function searchAllTrainings($search){
  $dbh = DB::connect();

  $query = "SELECT distinct tr_training.id, tr_training.title, tr_training.description, tr_training.responsible, tr_training_group.position, TO_CHAR(created, 'YYYY-MM-DD') AS created, TO_CHAR(updated, 'YYYY-MM-DD') AS updated FROM tr_training_group
  left join tr_training on tr_training_group.training_id = tr_training.id
   where ";
  $admin = '';
  if(array_key_exists("admin",$search) && $search["admin"]) {
    $query .= "responsible = :admin";
    $admin = strtolower($search["admin"]);
  }
  $text = '';
  if(array_key_exists("text",$search) && $search["text"]) {
    if($admin) {
      $query .= " and ";
    }
    $query .= "(lower(description) LIKE :text OR lower(title) LIKE :text)";
    $text = "%".strtolower($search["text"])."%";
  }
  $groupId = null;
  if(array_key_exists("groupId",$search) && $search["groupId"]) {
    if($admin OR $text) {
      $query .= " and ";
    }
    $query .= " group_id = :groupId";
    $groupId = $search["groupId"];
  }
    $query .= " order by tr_training_group.position";

  $q = $dbh->prepare($query);

  $q->bindParam(':admin', $admin, PDO::PARAM_STR, 100);
  $q->bindParam(':text', $text, PDO::PARAM_STR, 100);
  $q->bindParam(':groupId', $groupId, PDO::PARAM_INT, 10);

  if ($q->execute()) {
    $trainings = $q->fetchAll(PDO::FETCH_ASSOC);
    foreach ($trainings as &$training) {
      $training["groups"] = getTrainingGroups($training["ID"]);
      $training["courses"] = getTrainingCourses($training["ID"]);
    }
    return Utils::array_change_key_case_recursive($trainings);
  } else {
    Http::send_error(
      'Error while getting trainings.',
      $q->errorInfo()
    );
  }

}

function getTraining($trainingId){

  $dbh = DB::connect();

  $q = $dbh->prepare("SELECT id, title, description, responsible, TO_CHAR(created, 'YYYY-MM-DD') AS created, TO_CHAR(updated, 'YYYY-MM-DD') AS updated FROM tr_training where id = :trainingId");
  $q->bindParam(':trainingId', $trainingId, PDO::PARAM_INT, 10);

  if ($q->execute()) {
    $training = $q->fetch(PDO::FETCH_ASSOC);
    $training["groups"] = getTrainingGroups($training["ID"]);
    $training["courses"] = getTrainingCourses($training["ID"]);

    return Utils::array_change_key_case_recursive($training);
  } else {
    Http::send_error(
      'Error while getting trainings.',
      $q->errorInfo()
    );
  }

}

function createTraining($params){

  $dbh = DB::connect();

  try {
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $dbh->beginTransaction();

    $title = Utils::remove_accents($params["title"])?: Http::send_error("You need to provide a test name.");
    $description = Utils::remove_accents($params["description"])?: '';
    $responsible = $params["responsible"]?: User::current();

    $q = $dbh->prepare("
      INSERT INTO tr_training (title, description, responsible)
      VALUES (:title, :description, :responsible)
      RETURNING id INTO :trainingId
    ");
    $q->bindParam(':title', $title, PDO::PARAM_STR, 50);
    $q->bindParam(':description', $description, PDO::PARAM_STR, strlen($description));
    $q->bindParam(':responsible', $responsible, PDO::PARAM_STR, 20);
    $q->bindParam(':trainingId', $trainingId, PDO::PARAM_INT, 10);

    if ($q->execute()) {

      if($params["courses"]){
        $query = 'INSERT ALL ';
        $pos = 1;
        foreach ($params["courses"] as $course) {
          $edmsId = intval($course);
          $query .= "INTO tr_training_course (training_id, course_edms, position) VALUES (:trainingId, $edmsId, $pos) ";
          $pos++;
        }
        $query .= 'SELECT 1 FROM DUAL';
        $q2 = $dbh->prepare($query);

        $q2->bindParam(':trainingId', $trainingId, PDO::PARAM_INT, 10);

        if (!$q2->execute()) {
          Http::send_error(
            'Error while creating new training.',
            $q2->errorInfo()
          );
        }
      }

      if($params["groups"]){
        $query = 'INSERT ALL ';
        foreach ($params["groups"] as $group) {
          $groupId = intval($group);
          $query .= "INTO tr_training_group (training_id, group_id) VALUES (:trainingId, $groupId) ";
        }
        $query .= 'SELECT 1 FROM DUAL';
        $q3 = $dbh->prepare($query);

        $q3->bindParam(':trainingId', $trainingId, PDO::PARAM_INT, 10);

        if (!$q3->execute()) {
          Http::send_error(
            'Error while creating new training.',
            $q3->errorInfo()
          );
        }
      }

    }

    $dbh->commit();
    Http::send_created( array('id' => $trainingId ));
  } catch (Exception $e) {
    $dbh->rollBack();
    Http::send_error(
      'Error while creating new training.',
      $q->errorInfo()
    );
  }

}

function updateTraining($trainingId, $params){
  $dbh = DB::connect();

  try {
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $dbh->beginTransaction();

    $q = $dbh->prepare("
      UPDATE tr_training
      SET title = :title,
          description = :description,
          responsible = :responsible
      WHERE id = :trainingId
    ");

    $q->bindParam(':title', $params["title"], PDO::PARAM_STR, 50);
    $q->bindParam(':description', $params["description"], PDO::PARAM_STR, 200);
    $q->bindParam(':responsible', $params["responsible"], PDO::PARAM_STR, 20);
    $q->bindParam(':trainingId', $trainingId, PDO::PARAM_INT, 10);

    if ($q->execute()) {

      if($params["courses"]){
        $q2 = $dbh->prepare("DELETE FROM tr_training_course WHERE training_id = :trainingID");
        $q2->bindParam(':trainingID', $trainingId, PDO::PARAM_INT, 10);
        $q2->execute();

        $query = 'INSERT ALL ';
        $pos = 1;
        foreach ($params["courses"] as $course) {
          $edmsId = intval($course);
          $query .= "INTO tr_training_course (training_id, course_edms, position) VALUES (:trainingId, $edmsId, $pos) ";
          $pos++;
        }
        $query .= 'SELECT 1 FROM DUAL';
        $q3 = $dbh->prepare($query);

        $q3->bindParam(':trainingId', $trainingId, PDO::PARAM_INT, 10);

        if (!$q3->execute()) {
          Http::send_error(
            'Error while creating new training.',
            $q3->errorInfo()
          );
        }
      }

      if($params["groups"]){
        $q3 = $dbh->prepare("DELETE FROM tr_training_group WHERE training_id = :trainingID");
        $q3->bindParam(':trainingID', $trainingId, PDO::PARAM_INT, 10);

        $q3->execute();

        $query = 'INSERT ALL ';
        foreach ($params["groups"] as $group) {
          $groupId = intval($group);
          $query .= "INTO tr_training_group (training_id, group_id) VALUES (:trainingId, $groupId) ";
        }
        $query .= 'SELECT 1 FROM DUAL';
        $q4 = $dbh->prepare($query);

        $q4->bindParam(':trainingId', $trainingId, PDO::PARAM_INT, 10);

        if (!$q4->execute()) {
          Http::send_error(
            'Error while creating new training.',
            $q4->errorInfo()
          );
        }
      }
    }

    $dbh->commit();
    Http::send_ok("Training data has been updated.");
  } catch (Exception $e) {
    $dbh->rollBack();
    Http::send_error(
      'Error while updating training.',
      $q->errorInfo()
    );
  }
}

function deleteTraining($trainingId) {

  $dbh = DB::connect();

  try {
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $dbh->beginTransaction();

    $q = $dbh->prepare("DELETE FROM tr_training_course WHERE training_id = :trainingID");
    $q->bindParam(':trainingID', $trainingId, PDO::PARAM_INT, 10);

    $q->execute();

    $q2 = $dbh->prepare("DELETE FROM tr_training_group WHERE training_id = :trainingID");
    $q2->bindParam(':trainingID', $trainingId, PDO::PARAM_INT, 10);

    $q2->execute();

    $q3 = $dbh->prepare("DELETE FROM tr_classroom_user_course WHERE training_id = :trainingID");
    $q3->bindParam(':trainingID', $trainingId, PDO::PARAM_INT, 10);

    $q3->execute();

    $q4 = $dbh->prepare("DELETE FROM tr_training WHERE ID = :trainingID");
    $q4->bindParam(':trainingID', $trainingId, PDO::PARAM_INT, 10);

    $q4->execute();

    $dbh->commit();
    Http::send_deleted();
  } catch (Exception $e) {
    $dbh->rollBack();
    Http::send_error(
      'Error while creating new training.',
      $q->errorInfo()
    );
  }

}

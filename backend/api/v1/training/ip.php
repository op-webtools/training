<?PHP

$route->get('/ip', function () {
  ipInfo();
});

function ipInfo() {
  $remoteIP = $_SERVER['REMOTE_ADDR'];

  $CERN_TECHNET_MASK = "172.18.";

  $CERN_IP_LIST = array(
    '10.',
    '128.141.',
    '128.142.',
    '137.138.',
    '172.16.',
    '172.17.',
    '172.18.',
    '192.16.155.',
    '192.16.165.',
    '192.91.242.',
    '192.168.',
    '194.12.128.'
  );

  if(strpos ($remoteIP,$CERN_TECHNET_MASK)===0){
    $TECHNET = true;
    $CERN = true;
  }else{
    $TECHNET = false;
    $CERN = false;
    foreach($CERN_IP_LIST as $IP_CERN){
      if ( strpos ($remoteIP,$IP_CERN)===0){
        $CERN=TRUE;
        break;
      }
    }
  }
  Http::send_json( array('CERN' => $CERN, 'TECHNET' => $TECHNET ));
}
?>

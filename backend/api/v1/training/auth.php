<?php

User::init();

/**
 * @SWG\Definition(
 *     definition="User",
 *     type="object",
 *     @SWG\Property(property="office", type="string"),
 *     @SWG\Property(property="phone", type="integer"),
 *     @SWG\Property(property="display_name", type="string"),
 *     @SWG\Property(property="user_id", type="string"),
 *     @SWG\Property(property="group", type="string"),
 *     @SWG\Property(property="lang", type="string"),
 *     @SWG\Property(property="first_name", type="string"),
 *     @SWG\Property(property="email", type="string"),
 *     @SWG\Property(property="company", type="string"),
 *     @SWG\Property(property="account_type", type="string"),
 *     @SWG\Property(property="last_name", type="string"),
 *     @SWG\Property(property="cern_id", type="integer"),
 *     @SWG\Property(property="division", type="string"),
 *     @SWG\Property(property="section", type="string"),
 *     @SWG\Property(property="department", type="string")
 * )
 */

/**
 * @SWG\Get(
 *     path="/users/current",
 *     summary="Get current user data.",
 *     tags={"All", "Users"},
 *     operationId="getCurrentUserData",
 *     produces={
 *         "application/json"
 *     },
 *     @SWG\Response(
 *         response=200,
 *         description="Logged in user details.",
 *         @SWG\Schema(ref="#/definitions/User")
 *     )
 * )
 */
$route->get('/users/current', function () {
  $user = User::person(User::current());
  if($user) $user['is_admin'] = Utils::isMainAdmin();
  Http::send_json($user);
});

/**
 * @SWG\Get(
 *     path="/users/{userId}",
 *     summary="Get user data.",
 *     tags={"All", "Users"},
 *     operationId="getUserData",
 *     produces={
 *         "application/json"
 *     },
 *     @SWG\Response(
 *         response=200,
 *         description="User details.",
 *         @SWG\Schema(ref="#/definitions/User")
 *     )
 * )
 */
$route->get('/users/:string', function ($userID) {
  Utils::checkIfUserExists($userID);
  Http::send_json(User::person($userID));
});

/**
 * @SWG\Get(
 *     path="/search/users",
 *     summary="Search for users and/or egroups.",
 *     tags={"All", "Users"},
 *     operationId="searchPeopleEgroups",
 *     produces={
 *         "application/json"
 *     },
 *     @SWG\Parameter(
 *         in="query",
 *         name="name",
 *         required=true,
 *         type="string"
 *     ),
 *     @SWG\Parameter(
 *         in="query",
 *         name="users",
 *         required=false,
 *         type="boolean"
 *     ),
 *     @SWG\Parameter(
 *         in="query",
 *         name="egroups",
 *         required=false,
 *         type="boolean"
 *     ),
 *     @SWG\Response(
 *         response=200,
 *         description="List of results.",
 *         @SWG\Schema(
 *             type="array",
 *             @SWG\Items(ref="#/definitions/UserOrEgroup")
 *         )
 *     )
 * )
 */
$route->get('/search/users', function ($params) {
  $res = [];
  if (isset($params["egroups"]) && $params["egroups"] === "true") {
    $res = array_merge($res, User::egroup_search($params["name"]));
  }
  if (isset($params["users"]) && $params["users"] === "true") {
    $res = array_merge($res, User::search($params["name"]));
  }
  Http::send_json($res);
});

/**
 * @SWG\Get(
 *     path="/egroups/{egroupId}/members",
 *     summary="Get list of egroup member.",
 *     tags={"All", "Users"},
 *     operationId="getEgroupMembers",
 *     produces={
 *         "application/json"
 *     },
 *     @SWG\Parameter(
 *         in="path",
 *         name="egroupId",
 *         required=true,
 *         type="string"
 *     ),
 *     @SWG\Response(
 *         response=200,
 *         description="List of egroup member.",
 *         @SWG\Schema(ref="#/definitions/User")
 *     )
 * )
 */
$route->get('/egroups/:string/members', function ($q) {
  Http::send_json(User::egroup_members($q));
});

/**
 * @SWG\Get(
 *     path="/users/current/valreq",
 *     summary="Get classroom validation requests",
 *     tags={"All", "Classrooms"},
 *     operationId="getClassroomValreq",
 *     produces={
 *         "application/json"
 *     },
 *     @SWG\Parameter(
 *         in="path",
 *         name="classroomId",
 *         required=true,
 *         type="integer"
 *     ),
 *     @SWG\Response(
 *         response=200,
 *         description="Classroom validation requests.",
 *         @SWG\Schema(
 *             type="array",
 *             @SWG\Item(type="string")
 *         )
 *     )
 * )
 */
$route->get(
  '/users/current/valreq',
  function() {
    getCurrentUserValreq();
  }
);

function getCurrentUserValreq(){

  $dbh = DB::connect();
  $current = User::current();

  $q = $dbh->prepare("
    SELECT  uc.user_id, uc.course_edms, cuc.responsible
    FROM tr_user_course uc
    LEFT JOIN tr_classroom_user_course cuc 
    ON uc.user_id = cuc.user_id 
    AND uc.course_edms = cuc.course_edms
    WHERE cuc.responsible LIKE '%$current%'
    AND uc.date_start IS NOT NULL
    AND uc.date_end IS NOT NULL
    AND uc.validated_by IS NULL
  ");

  if ($q->execute()) {
    $data = [];
    foreach (Utils::array_change_key_case_recursive($q->fetchAll(PDO::FETCH_ASSOC)) as $res) {
      if (!in_array($current, explode(',', $res['responsible']))) continue;
      if (in_array($res['course_edms'], array_map(function($d) { return $d['course_edms'];}, $data ))) continue;
      $data[] = $res;
    }
    Http::send_json($data);
  } else {
    Http::send_error(
      'Error while getting current user validation requests.',
      $q->errorInfo()
    );
  }
};

class User
{
  private static $ldapconn;
  private static $current;
  private static $info;
  private static $attributes;
  private static $attributes_short;

  // const CERN_USER_BASE = 'DC=cern,DC=ch';
  const CERN_USER_BASE = 'OU=Users,OU=Organic Units,DC=cern,DC=ch';
  const CERN_GROUP_BASE = 'OU=e-groups,OU=Workgroups,DC=cern,DC=ch';
  // Used to query if an account is a member of a specific e-group. Arguments are the account name, the group name, and CERN_GROUP_BASE
  const CERN_GROUP_MEMBERSHIP_QUERY = '(&(objectClass=user)(sAMAccountName=%s)(memberOf:1.2.840.113556.1.4.1941:=CN=%s,%s))';
  // Used to query all the e-groups of an account. Arguments are ROLE_NAME_PREFIX, the account name, and CERN_USER_BASE
  const CERN_ACCOUNT_GROUPS_QUERY = '(&(CN=%s*)(member=CN=%s,%s))';
  // Used to query the properties of an account. Arguments are the account name, and CERN_USER_BASE
  // const CERN_ACCOUNT_QUERY = '(&(objectClass=user)(CN=%s))';
  const CERN_ACCOUNT_QUERY = '(&(objectClass=user)(uidNumber=*)(cernaccounttype=Primary)(!(division=Other))(CN=%s))';
  // Used to query by first or last name.
  const CERN_NAME_QUERY_SIMPLE = '(&(objectClass=user)(uidNumber=*)(cernaccounttype=Primary)(!(division=Other))(|(sn=%s)(givenName=%s)))';
  // Used to query by first and last name.
  const CERN_NAME_QUERY_FIRST_AND_LAST = '(&(objectClass=user)(uidNumber=*)(cernaccounttype=Primary)(!(division=Other)) (|(&(sn=%s)(givenName=%s))(&(givenName=%s)(sn=%s))) )';

  public static function init()
  {
    self::$ldapconn = ldap_connect('xldap.cern.ch');

    if(array_key_exists('OIDC_CLAIM_cern_upn', $_SERVER)) {
      $username = $_SERVER['OIDC_CLAIM_cern_upn'];
    } else if (array_key_exists('REDIRECT_OIDC_CLAIM_cern_upn', $_SERVER)) {
      $username = $_SERVER['REDIRECT_OIDC_CLAIM_cern_upn'];
    } else if (array_key_exists('OIDC_CLAIM_preferred_username', $_SERVER)) {
      $username = $_SERVER['OIDC_CLAIM_preferred_username'];
    } else if (array_key_exists('REMOTE_USER', $_SERVER)) {
      $username = $_SERVER['REMOTE_USER'];
    } else if (array_key_exists('REMOTE_USER', $_ENV)) {
      $username = $_ENV['REMOTE_USER'];
    } else {
      $username = NULL;
    }
    if (!empty($username)) {
      self::$current = str_replace('CERN\\', '', $username);
    }
    self::$attributes = [
        "name" => "user_id",
        "givenname" => "first_name",
        "displayname" => "display_name",
        "sn" => "last_name",
        "employeeid" => "cern_id",
        "physicaldeliveryofficename" => "office",
        "preferredlanguage" => "lang",
        "mail" => "email",
        "company" => "company",
        "cernaccounttype" => "account_type",
        "division" => "division",
        "cerngroup" => "group",
        "cernsection" => "section",
        "telephonenumber" => "phone",
        "mobile" => "mobile"
    ];
    self::$attributes_short = [
        "name" => "user_id",
        "displayname" => "display_name",
        "department" => "department",
        "employeeid" => "cern_id",
        "telephonenumber" => "phone",
        "mobile" => "mobile",
        "mail" => "email"
    ];

  }

  public static function current()
  {
    return(self::$current);
  }

  public static function person($user_id, $short = false)
  {
    $user_id = strtoupper($user_id);
    if($short){
      $filter = self::$attributes_short;
    }else{
      $filter = self::$attributes;
    }
    $query = sprintf(self::CERN_ACCOUNT_QUERY, $user_id);
    $search = ldap_search(self::$ldapconn, self::CERN_USER_BASE, $query,array_keys($filter));
    $result = ldap_get_entries(self::$ldapconn, $search);
    $valid = $result['count'] > 0;

    $info = [];
    if($valid) {
      foreach($result[0] as $k => $v){
        if(array_key_exists($k, $filter)){
          $info[$filter[$k]] = $v[0];
        }
      }
    }

    if ($short) {
      unset($info['cern_id']);
    }
    // Add SSO Roles
    if(array_key_exists('OIDC_CLAIM_cern_roles', $_SERVER))
    {
      $info['roles'] = $_SERVER['OIDC_CLAIM_cern_roles'];
    }
    return($info);
  }

  public static function search($q)
  {
    if(strlen($q) < 2)return;

    $parts = array_map("trim", explode(" ", $q));

    switch (count($parts)) {
      case 1:
        $query = sprintf(self::CERN_NAME_QUERY_SIMPLE, "*$q*", "*$q*");
        break;
      case 2:
        $query = sprintf(self::CERN_NAME_QUERY_FIRST_AND_LAST, "*$parts[0]*", "*$parts[1]*", "*$parts[0]*", "*$parts[1]*");
        break;
      case 3:
        $query = sprintf(self::CERN_NAME_QUERY_FIRST_AND_LAST, "*$parts[0] $parts[1]*", "$parts[2]*", "*$parts[0] $parts[1]*", "$parts[2]*");
        break;
    }

    // $query = sprintf(self::CERN_NAME_QUERY_SIMPLE, "*$q*", "*$q*");

    $search = ldap_search(self::$ldapconn, self::CERN_USER_BASE, $query,array_keys(self::$attributes_short));
    $results = ldap_get_entries(self::$ldapconn, $search);
    $valid = $results['count'] > 0;
    unset($results["count"]);
    if($valid) {
      foreach ($results as $key => $value)
      {
        $info = [];
        foreach($value as $k => $v)
        {
          if(array_key_exists($k, self::$attributes_short)){
            // if($k == "givenname"){
            //   $v[0] = explode(" ", $v[0])[0];
            // }
            $info[self::$attributes_short[$k]] = $v[0];
          }
        }
        $results[$key] = $info;
      }
      return($results);
    }
    return [];
  }

  /**
   * Returns TRUE if this user is a member of the CERN e-group by the name of $groupname, and FALSE otherwise.
   * This function take recursive group membership into account.
   */
  public static function is_egroup_member($groupname)
  {
    $query = sprintf(self::CERN_GROUP_MEMBERSHIP_QUERY, ldap_escape(self::$current), ldap_escape($groupname), self::CERN_GROUP_BASE);
    $search = ldap_search(self::$ldapconn, self::CERN_USER_BASE, $query, ['cn']);
    return (ldap_count_entries(self::$ldapconn, $search) > 0);
  }

  /**
   * Returns the list of members of an egroup passed as argument.
   * This function doesn't take recursive group membership into account.
   */
  public static function egroup_members($groupName)
  {
    $query = "(&(objectClass=user)(memberOf:=CN=$groupName,OU=e-groups,OU=Workgroups,DC=cern,DC=ch))";
    $search = ldap_search(self::$ldapconn, self::CERN_USER_BASE, $query, array_keys(self::$attributes));
    $result = ldap_get_entries(self::$ldapconn, $search);
    $results = array();
    unset($result["count"]);

    foreach ($result as $key => $value)
    {
      $info = [];
      foreach($value as $k => $v)
      {
        if(array_key_exists($k, self::$attributes)){
          if($k == "givenname"){
            $v[0] = explode(" ", $v[0])[0];
          }
          $info[self::$attributes[$k]] = $v[0];
        }
      }
      $results[$key] = $info;
    }

    return($results);
  }

  public static function egroup_search($q)
  {
    $res = array();

    $query = "(cn=*$q*)";
    // $query = "(&(objectClass=group)(CN=$q*,OU=e-groups,OU=Workgroups,DC=cern,DC=ch))";
    $search = ldap_search(self::$ldapconn, self::CERN_GROUP_BASE, $query, ["name", "description"]);
    $results = ldap_get_entries(self::$ldapconn, $search);
    unset($results["count"]);
    foreach ($results as $result) {
      $res[] = array(
        // TODO check if add '@' for dispaly_name and add a field
          "user_id"=>$result['name'][0],
          "description"=>$result['description'][0]
      );
    }
    return $res;
  }

  public static function is_egroup($q)
  {

    $query = "(cn=$q)";
    // $query = "(&(objectClass=group)(CN=$q*,OU=e-groups,OU=Workgroups,DC=cern,DC=ch))";
    $search = ldap_search(self::$ldapconn, self::CERN_GROUP_BASE, $query, ["name"]);
    $results = ldap_get_entries(self::$ldapconn, $search);
    return $results["count"] > 0;
  }
}

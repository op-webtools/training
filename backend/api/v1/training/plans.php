<?php

// ROUTES --- --- ---

/**
 * @SWG\Get(
 *     path="/plans",
 *     summary="Get list of plans",
 *     tags={"All", "Plans"},
 *     operationId="getPlans",
 *     @SWG\Parameter(
 *         in="path",
 *         name="responsible",
 *         required=false,
 *         type="string"
 *     ),
 *     @SWG\Parameter(
 *         in="path",
 *         name="trainee",
 *         required=false,
 *         type="string"
 *     ),
 *     produces={
 *         "application/json"
 *     },
 *     @SWG\Response(
 *         response=200,
 *         description="List of plans.",
 *         @SWG\Schema(
 *             type="array",
 *             @SWG\Items(
 *                 type="object",
 *                 @SWG\Property(property="ID", type="integer"),
 *                 @SWG\Property(property="RESPONSIBLE", type="string"),
 *                 @SWG\Property(property="TRAINEE", type="string")
 *             )
 *         )
 *     )
 * )
 */
$route->get(
  '/plans',
  function($params) {
    getPlans($params);
  }
);

/**
 * @SWG\Get(
 *     path="/plans/validate",
 *     summary="Get list of courses to validate",
 *     tags={"All", "Plans"},
 *     operationId="getValidatorCourses",
 *     produces={
 *         "application/json"
 *     },
 *     @SWG\Response(
 *         response=200,
 *         description="List of courses to validate.",
 *         @SWG\Schema(
 *             type="array",
 *             @SWG\Items(
 *                 type="object",
 *                 @SWG\Property(property="PLAN_ID", type="integer"),
 *                 @SWG\Property(property="TRAINING_ID", type="integer"),
 *                 @SWG\Property(property="COURSE_ID", type="integer"),
 *                 @SWG\Property(property="TRAINEE", type="string"),
 *                 @SWG\Property(property="TITLE", type="string")
 *             )
 *         )
 *     )
 * )
 */
$route->get(
  '/plans/validate',
  function() {
    Http::send_json(getValidatorCourses());
  }
);

/**
 * @SWG\Get(
 *     path="/plans/{planId}",
 *     summary="Get plan data",
 *     tags={"All", "Plans"},
 *     operationId="getplan",
 *     produces={
 *         "application/json"
 *     },
 *     @SWG\Parameter(
 *         in="path",
 *         name="planId",
 *         required=true,
 *         type="integer"
 *     ),
 *     @SWG\Response(
 *         response=200,
 *         description="plan data.",
 *         @SWG\Schema(
 *            @SWG\Property(property="ID", type="integer"),
 *            @SWG\Property(property="RESPONSIBLE", type="string"),
 *            @SWG\Property(property="TRAINEE", type="string")
 *         )
 *     )
 * )
 */
$route->get(
  '/plans/:int',
  function($planId) {
    Http::send_json(getplan($planId));
  }
);

/**
 * @SWG\Get(
 *     path="/plans/{planId}/courses",
 *     summary="Get courses for plan",
 *     tags={"All", "Plans", "Courses"},
 *     operationId="getPlansCourses",
 *     produces={
 *         "application/json"
 *     },
 *     @SWG\Parameter(
 *         in="path",
 *         name="planId",
 *         required=true,
 *         type="string"
 *     ),
 *     @SWG\Response(
 *         response=200,
 *         description="List of courses.",
 *         @SWG\Schema(
 *             type="array",
 *               @SWG\Items(
 *                @SWG\Property(property="GROUP_ID", type="integer"),
 *                @SWG\Property(property="TRAINING_ID", type="integer"),
 *                @SWG\Property(property="ID", type="string"),
 *                @SWG\Property(property="TITLE", type="string"),
 *                @SWG\Property(property="EDMS", type="integer"),
 *                @SWG\Property(property="STARTED", type="string"),
 *                @SWG\Property(property="COMPLETED", type="string"),
 *                @SWG\Property(property="VALIDATION_BY", type="array"),
 *                @SWG\Property(property="DEADLINE", type="string"),
 *                @SWG\Property(property="VALIDATION", type="integer"),
 *                @SWG\Property(property="VALIDATION_DATE", type="string"),
 *                @SWG\Property(property="EXPIRATION_DATE", type="string")
 *             )
 *           )
 *        )
 *    )
 * )
 */
$route->get(
  '/plans/:int/courses',
  function($planId) {
    getPlansCourses($planId);
  }
);


/**
 * @SWG\Put(
 *     path="/plans/{planId}/trainings/{trainingId}/courses/{courseId}",
 *     summary="Update course data for plan",
 *     tags={"All", "Plans", "Courses"},
 *     operationId="updatePlanCourse",
 *     produces={
 *         "application/json"
 *     },
 *     @SWG\Parameter(
 *         in="path",
 *         name="planId",
 *         required=true,
 *         type="integer"
 *     ),
 *     @SWG\Parameter(
 *         in="path",
 *         name="trainingId",
 *         required=true,
 *         type="integer"
 *     ),
 *     @SWG\Parameter(
 *         in="path",
 *         name="courseId",
 *         required=true,
 *         type="integer"
 *     ),
 *     @SWG\Parameter(
 *         in="body",
 *         name="body",
 *         required=true,
 *         schema=@SWG\Schema(
 *            @SWG\Property(property="STARTED", type="string"),
 *            @SWG\Property(property="COMPLETED", type="string"),
 *            @SWG\Property(property="DEADLINE", type="string"),
 *            @SWG\Property(property="VALIDATION_BY", type="string"),
 *            @SWG\Property(property="VALIDATION_DATE", type="string")
 *         )
 *     ),
 *     @SWG\Response(
 *         response=200,
 *         description="Update course."
 *     )
 * )
 */
$route->put(
  '/plans/:int/trainings/:int/courses/:int',
  function($planId, $trainingId, $courseId, $params) {
    if(isPlanResponsible($planId)){
      updatePlanCourse($planId, $trainingId, $courseId, $params);
    }else{
      Utils::sendPrivligesErr();
    }
  }
);

/**
 * @SWG\Post(
 *     path="/plans",
 *     summary="Create new plan",
 *     tags={"All", "Plans"},
 *     operationId="createPlan",
 *     produces={
 *         "application/json"
 *     },
 *     @SWG\Parameter(
 *         in="body",
 *         name="body",
 *         required=true,
 *         schema=@SWG\Schema(
 *            @SWG\Property(property="RESPONSIBLE", type="string"),
 *            @SWG\Property(property="TRAINEE", type="string")
 *         )
 *     ),
 *     @SWG\Response(
 *         response=200,
 *         description="plan has been created.",
 *         @SWG\Schema(
 *             @SWG\Property(property="ID", type="integer")
 *         )
 *     )
 * )
 */
$route->post(
  '/plans',
  function($params) {
    if (isAdmin()) {
      createPlan($params);
    } else {
      Utils::sendPrivligesErr();
    }
  }
);

/**
 * @SWG\Post(
 *     path="/plans/{planId}/courses",
 *     summary="Add courses to plan",
 *     tags={"All", "Plans", "Courses"},
 *     operationId="addCoursesToPlan",
 *     produces={
 *         "application/json"
 *     },
 *     @SWG\Parameter(
 *         in="path",
 *         name="planId",
 *         required=true,
 *         type="integer"
 *     ),
 *     @SWG\Parameter(
 *         in="body",
 *         name="body",
 *         required=true,
 *         @SWG\Schema(
 *             type="array",
 *             @SWG\Items(
 *                 type="object",
 *                  @SWG\Property(property="TRAINING_ID", type="integer"),
 *                  @SWG\Property(property="ID", type="integer"),
 *                  @SWG\Property(property="DEADLINE", type="string"),
 *                  @SWG\Property(property="VALIDATION_BY", type="string")
 *             )
 *         )
 *     ),
 *     @SWG\Response(
 *         response=200,
 *         description="Courses have been added."
 *     )
 * )
 */
$route->post(
  '/plans/:int/courses',
  function($planId, $params) {
    if (isAdmin()) {
      addCoursesToPlan($planId, $params);
    } else {
      Utils::sendPrivligesErr();
    }
  }
);

/**
 * @SWG\Post(
 *     path="/plans/{planId}/courses/{courseId}/start",
 *     summary="Set start date for course in plan",
 *     tags={"All", "Plans", "Courses"},
 *     operationId="startPlanCourse",
 *     produces={
 *         "application/json"
 *     },
 *     @SWG\Parameter(
 *         in="path",
 *         name="planId",
 *         required=true,
 *         type="integer"
 *     ),
 *     @SWG\Parameter(
 *         in="path",
 *         name="courseId",
 *         required=true,
 *         type="integer"
 *     ),
 *     @SWG\Response(
 *         response=200,
 *         description="Course Start date has been updated."
 *     )
 * )
 */
$route->post(
  '/plans/:int/courses/:int/start',
  function($planId, $courseId) {
    if (isPlanTrainee($planId)) {
      startPlanCourse($planId, $courseId);
    } else {
      Http::send_error("This is not your course");
    }
  }
);

/**
 * @SWG\Post(
 *     path="/plans/{planId}/courses/{courseId}/complete",
 *     summary="Set complete date for course in plan",
 *     tags={"All", "Plans", "Courses"},
 *     operationId="completePlanCourse",
 *     produces={
 *         "application/json"
 *     },
 *     @SWG\Parameter(
 *         in="path",
 *         name="planId",
 *         required=true,
 *         type="integer"
 *     ),
 *     @SWG\Parameter(
 *         in="path",
 *         name="courseId",
 *         required=true,
 *         type="integer"
 *     ),
 *     @SWG\Response(
 *         response=200,
 *         description="Course Completed date has been updated."
 *     )
 * )
 */
$route->post(
  '/plans/:int/courses/:int/complete',
  function($planId, $courseId) {
    if (isPlanTrainee($planId)) {
      completePlanCourse($planId, $courseId);
    } else {
      Http::send_error("This is not your course");
    }
  }
);

/**
 * @SWG\Post(
 *     path="/plans/{planId}/trainings/{trainingId}/courses/{courseId}/validate",
 *     summary="Validate course in plan",
 *     tags={"All", "Plans", "Courses"},
 *     operationId="validatePlanCourse",
 *     produces={
 *         "application/json"
 *     },
 *     @SWG\Parameter(
 *         in="path",
 *         name="planId",
 *         required=true,
 *         type="integer"
 *     ),
 *     @SWG\Parameter(
 *         in="path",
 *         name="trainingId",
 *         required=true,
 *         type="integer"
 *     ),
 *     @SWG\Parameter(
 *         in="path",
 *         name="courseId",
 *         required=true,
 *         type="integer"
 *     ),
 *     @SWG\Response(
 *         response=200,
 *         description="Course validated."
 *     )
 * )
 */
$route->post(
  '/plans/:int/trainings/:int/courses/:int/validate',
  function($planId, $trainingId, $courseId) {
    if (isCourseValidator($planId, $trainingId, $courseId)) {
      validatePlanCourse($planId, $trainingId, $courseId);
    } else {
      Http::send_error("You are not allowed to validate this course");
    }
  }
);

/**
 * @SWG\Post(
 *     path="/plans/{planId}/courses/{courseId}/askValidation",
 *     summary="ask validator to validate the course",
 *     tags={"All", "Plans", "Courses"},
 *     operationId="askValidation",
 *     produces={
 *         "application/json"
 *     },
 *     @SWG\Parameter(
 *         in="path",
 *         name="planId",
 *         required=true,
 *         type="integer"
 *     ),
 *     @SWG\Parameter(
 *         in="path",
 *         name="trainingId",
 *         required=true,
 *         type="integer"
 *     ),
 *     @SWG\Parameter(
 *         in="path",
 *         name="courseId",
 *         required=true,
 *         type="integer"
 *     ),
 *     @SWG\Response(
 *         response=200,
 *         description="Course validation requested."
 *     )
 * )
 */
$route->post(
  '/plans/:int/courses/:int/askValidation',
  function($planId, $courseId) {
    if (isPlanTrainee($planId)) {
      askValidation($planId, $courseId);
    } else {
      Http::send_error("You are not allowed to ask validation for this course");
    }
  }
);

/**
 * @SWG\Delete(
 *     path="/plans/{planId}/trainings/{trainingId}/courses/{courseId}",
 *     summary="Delete course from plan",
 *     tags={"All", "Plans", "Courses"},
 *     operationId="deletePlanCourses",
 *     produces={
 *         "application/json"
 *     },
 *     @SWG\Parameter(
 *         in="path",
 *         name="planId",
 *         required=true,
 *         type="integer"
 *     ),
 *     @SWG\Parameter(
 *         in="path",
 *         name="trainingId",
 *         required=true,
 *         type="integer"
 *     ),
 *     @SWG\Parameter(
 *         in="path",
 *         name="courseId",
 *         required=true,
 *         type="integer"
 *     ),
 *     @SWG\Response(
 *         response=200,
 *         description="Course has been deleted."
 *     )
 * )
 */
$route->delete(
  '/plans/:int/trainings/:int/courses/:int',
  function($planId, $trainingId, $courseId) {
    if (isAdmin()) {
      deletePlanCourses($planId, $trainingId, $courseId);
    } else {
      Utils::sendPrivligesErr();
    }
  }
);

/**
 * @SWG\Delete(
 *     path="/plans/trainings/{trainingId}/courses/{courseId}",
 *     summary="Delete course from all plans",
 *     tags={"All", "Plans", "Courses"},
 *     operationId="deleteCourseFromPlans",
 *     produces={
 *         "application/json"
 *     },
 *     @SWG\Parameter(
 *         in="path",
 *         name="trainingId",
 *         required=true,
 *         type="integer"
 *     ),
 *     @SWG\Parameter(
 *         in="path",
 *         name="courseId",
 *         required=true,
 *         type="integer"
 *     ),
 *     @SWG\Response(
 *         response=200,
 *         description="Course has been deleted from all plans."
 *     )
 * )
 */
$route->delete(
  '/plans/trainings/:int/courses/:int',
  function($trainingId, $courseId) {
    if (isAdmin()) {
      deleteCourseFromPlans( $trainingId, $courseId);
    } else {
      Utils::sendPrivligesErr();
    }
  }
);

/**
 * @SWG\Delete(
 *     path="/plans/trainings/{trainingId}/courses",
 *     summary="Delete training courses from all plans",
 *     tags={"All", "Plans", "Courses"},
 *     operationId="deleteTrainingCoursesFromPlans",
 *     produces={
 *         "application/json"
 *     },
 *     @SWG\Parameter(
 *         in="path",
 *         name="trainingId",
 *         required=true,
 *         type="integer"
 *     ),
 *     @SWG\Response(
 *         response=200,
 *         description="Training courses has been deleted from all plans."
 *     )
 * )
 */
$route->delete(
  '/plans/trainings/:int/courses',
  function($trainingId, $courseId) {
    if (isAdmin()) {
      deleteTrainingCoursesFromPlans( $trainingId);
    } else {
      Utils::sendPrivligesErr();
    }
  }
);


/**
 * @SWG\Put(
 *     path="/plans/{planId}",
 *     summary="Update plan data",
 *     tags={"All", "Plans"},
 *     operationId="updateplan",
 *     produces={
 *         "application/json"
 *     },
 *     @SWG\Parameter(
 *         in="path",
 *         name="planId",
 *         required=true,
 *         type="integer"
 *     ),
 *     @SWG\Parameter(
 *         in="body",
 *         name="body",
 *         required=true,
 *         schema=@SWG\Schema(
 *            @SWG\Property(property="RESPONSIBLE", type="string")
 *         )
 *     ),
 *     @SWG\Response(
 *         response=200,
 *         description="plan data has been updated."
 *     )
 * )
 */
$route->put(
  '/plans/:int',
  function($planId, $params) {
    if (isAdmin()) {
      updateplan($planId, $params["RESPONSIBLE"]);
    } else {
      Utils::sendPrivligesErr();
    }
  }
);

/**
 * @SWG\Delete(
 *     path="/plans/{planId}",
 *     summary="Delete plan",
 *     tags={"All", "Plans"},
 *     operationId="deleteplan",
 *     produces={
 *         "application/json"
 *     },
 *     @SWG\Parameter(
 *         in="path",
 *         name="planId",
 *         required=true,
 *         type="integer"
 *     ),
 *     @SWG\Response(
 *         response=204,
 *         description="Successfully deleted plan."
 *     )
 * )
 */
$route->delete(
  '/plans/:int',
  function($planId) {
    if (isAdmin()) {
      deletePlan($planId);
    } else {
      Utils::sendPrivligesErr();
    }
  }
);


function isCourseValidator($planId, $trainingId, $courseId){

  $dbh = DB::connect();

  $user = getUserName();
  $q = $dbh->prepare("
    SELECT COURSE_ID
    FROM tr_plan_course
    WHERE course_id = :course_id AND training_id = :training_id AND plan_id = :plan_id AND validation_by LIKE '%$user%' AND COMPLETED IS NOT null
  ");
  $q->bindParam(':course_id', $planId, PDO::PARAM_INT, 10);
  $q->bindParam(':training_id', $trainingId, PDO::PARAM_INT, 10);
  $q->bindParam(':plan_id', $courseId, PDO::PARAM_INT, 10);

  if ($q->execute()) {
    return $q->fetchAll(PDO::FETCH_ASSOC) > 0;
  } else {
    Http::send_error(
      'Error while getting plan.',
      $q->errorInfo()
    );
  }

}


function isPlanResponsible($planId) {
  return getplan($planId)["RESPONSIBLE"] === getUserName();
}

function isPlanTrainee($planId) {
  return getplan($planId)["TRAINEE"] === getUserName();
}
function getPlans($params){

  $query = "SELECT id, responsible, trainee FROM tr_plan ";

  if($params["responsible"] || $params["trainee"]){
    $query.= ' WHERE ';
  }
  if($params["responsible"]){
    $query.= 'responsible = :responsible';
  }
  if($params["responsible"] && $params["trainee"]){
    $query.= ' AND ';
  }
  if($params["trainee"]){
    $query.= 'trainee = :trainee';
  }
  $dbh = DB::connect();

  $q = $dbh->prepare($query);

  if($params["responsible"]){
    $q->bindParam(':responsible', $params["responsible"], PDO::PARAM_STR, 20);
  }
  if($params["trainee"]){
    $q->bindParam(':trainee', $params["trainee"], PDO::PARAM_STR, 20);
  }

  if ($q->execute()) {
    Http::send_json($q->fetchAll(PDO::FETCH_ASSOC));
  } else {
    Http::send_error(
      'Error while getting plans.',
      $q->errorInfo()
    );
  }

}

function getplan($planId){

  $dbh = DB::connect();

  $q = $dbh->prepare("
    SELECT id, responsible, trainee
    FROM tr_plan
    WHERE id = :planID
  ");
  $q->bindParam(':planID', $planId, PDO::PARAM_INT, 10);

  if ($q->execute()) {
    return $q->fetchAll(PDO::FETCH_ASSOC)[0];
  } else {
    Http::send_error(
      'Error while getting plan.',
      $q->errorInfo()
    );
  }

}

function getValidatorCourses(){

  $dbh = DB::connect();

  $user = getUserName();
  $q = $dbh->prepare("
    SELECT
    PLAN_ID, TRAINING_ID, COURSE_ID, TRAINEE, c.TITLE
    FROM tr_plan_course pc
    LEFT JOIN tr_plan p on pc.plan_id = p.id
    LEFT JOIN tr_course c on c.id = pc.course_id
    WHERE completed IS NOT null AND validation_date IS null AND validation_by LIKE '%$user%'
  ");
  // $q->bindParam(':user', $user, PDO::PARAM_STR);

  if ($q->execute()) {
    return $q->fetchAll(PDO::FETCH_ASSOC);
  } else {
    Http::send_error(
      'Error while getting plan.',
      $q->errorInfo()
    );
  }

}

function createPlan($params){

  $dbh = DB::connect();

  $trainee = $params["TRAINEE"]?: Http::send_error("You need to provide a Trainee's name.");
  $responsible = $params["RESPONSIBLE"]?: getUserName();

  $q = $dbh->prepare("
    INSERT INTO tr_plan (responsible, trainee)
    VALUES (:responsible, :trainee)
    RETURNING id INTO :planID
  ");

  $q->bindParam(':responsible', $responsible, PDO::PARAM_STR, 20);
  $q->bindParam(':trainee', $trainee, PDO::PARAM_STR, 20);
  $q->bindParam(':planID', $planId, PDO::PARAM_INT, 10);

  if ($q->execute()) {
    Http::send_created(array('ID' => $planId ));
  } else {
    Http::send_error(
      'Error while creating new plan.',
      $q->errorInfo()
    );
  }

}

function updateplan($planId, $responsible){
  $dbh = DB::connect();

  $q = $dbh->prepare("
    UPDATE tr_plan
    SET responsible = :responsible
    WHERE id = :planID
  ");

  $q->bindParam(':planID', $planId, PDO::PARAM_INT, 10);
  $q->bindParam(':responsible', $responsible, PDO::PARAM_STR, 20);

  if ($q->execute()) {
    Http::send_ok("plan data has been updated.");
  } else {
    Http::send_error(
      'Error while updating plan.',
      $q->errorInfo()
    );
  }

}

function deletePlan($planId){
  $dbh = DB::connect();

  $q = $dbh->prepare("
    DELETE FROM tr_plan
    WHERE id = :planID
  ");

  $q->bindParam(':planID', $planId, PDO::PARAM_INT, 10);

  if ($q->execute()) {
    Http::send_deleted();
  } else {
    Http::send_error(
      'Error while deleting plan.',
      $q->errorInfo()
    );
  }
}

function addCoursesToPlan($planId, $params){

  $dbh = DB::connect();
  $query = 'INSERT ALL ';
  for ($i=0; $i < count($params); $i++) {
    $query .= " INTO tr_plan_course (plan_id, training_id, course_id, deadline, validation_by) VALUES (:planId, :training_id_$i, :course_id_$i, TO_DATE(:deadline_$i, 'YYYY-MM-DD'), :validation_by_$i ) ";
  }
  $query .= 'SELECT 1 FROM DUAL';
  $q = $dbh->prepare($query);
  $i = 0;
  foreach ($params as &$course) {
    $q->bindParam(":course_id_$i", $course["ID"], PDO::PARAM_INT, 10);
    $q->bindParam(":training_id_$i", $course["TRAINING_ID"], PDO::PARAM_INT, 10);
    $deadline = $course["DEADLINE"]?:null;
    $q->bindParam(":deadline_$i", $deadline, PDO::PARAM_STR);
    $q->bindParam(":validation_by_$i", $course["VALIDATION_BY"], PDO::PARAM_STR);
    $i++;
  }
  $q->bindParam(':planId', $planId, PDO::PARAM_INT, 10);

  if ($q->execute()) {
    Http::send_created("courses have been added to training.");
  } else {
    Http::send_error(
      'Error while creating new training.',
      $q->errorInfo()
    );
  }

}

function getPlansCourses($planId){

  $dbh = DB::connect();
// TODO get order of curses!
  $q = $dbh->prepare("
    SELECT
    group_id,
    training_id,
    course_id AS ID,
    c.title,
    c.edms,
    TO_CHAR(started, 'YYYY-MM-DD') AS started,
    TO_CHAR(completed, 'YYYY-MM-DD') AS completed,
    validated_by,
    TO_CHAR(deadline, 'YYYY-MM-DD') AS deadline,
    validation_by,
    TO_CHAR(validation_date, 'YYYY-MM-DD') AS validation_date,
    TO_CHAR(expiration_date, 'YYYY-MM-DD') AS expiration_date
    FROM tr_plan_course pc
    LEFT JOIN tr_training t ON t.id = pc.training_id
    LEFT JOIN tr_course c ON c.ID = pc.COURSE_ID
    WHERE plan_id = :planId
    ORDER BY group_id, training_id, course_id
  ");
  $q->bindParam(':planId', $planId, PDO::PARAM_INT);

  if ($q->execute()) {
    Http::send_json($q->fetchAll(PDO::FETCH_ASSOC));
  } else {
    Http::send_error(
      'Error while getting plan courses.',
      $q->errorInfo()
    );
  }

}

function deletePlanCourses($planId, $trainingId, $courseId){

  $dbh = DB::connect();

  try {
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

      $q = $dbh->prepare("
      DELETE FROM tr_plan_course
      WHERE plan_id = :planId AND training_id = :trainingId AND course_id = :courseId
      ");
      $q->bindParam(':planId', $planId, PDO::PARAM_INT, 10);
      $q->bindParam(':trainingId', $trainingId, PDO::PARAM_INT, 10);
      $q->bindParam(':courseId', $courseId, PDO::PARAM_INT, 10);

      $q->execute();

    Http::send_deleted();
  } catch (Exception $e) {
    $dbh->rollBack();
    Http::send_error(
      'Error while deleting courses from plan.',
      "Error in line : {$e->getLine()} | {$e->getMessage()}"
    );
  }
};

function updatePlanCourse($planId, $trainingId, $courseId, $params) {
  $dbh = DB::connect();

  $params = whiteListParameters($params, [ "DEADLINE", "VALIDATION_BY"]);

  $q = $dbh->prepare("
    UPDATE tr_plan_course
    SET deadline = TO_DATE(:deadline, 'YYYY-MM-DD'),
        validation_by = :validation_by
    WHERE plan_id = :planId AND training_id = :trainingId AND course_id = :courseId
  ");

  $q->bindParam(':deadline', $params["DEADLINE"], PDO::PARAM_STR);
  $q->bindParam(':validation_by', $params["VALIDATION_BY"], PDO::PARAM_STR);
  $q->bindParam(':planId', $planId, PDO::PARAM_INT, 10);
  $q->bindParam(':trainingId', $trainingId, PDO::PARAM_INT, 10);
  $q->bindParam(':courseId', $courseId, PDO::PARAM_INT, 10);

  if ($q->execute()) {
    Http::send_ok("Course data has been updated.");
  } else {
    Http::send_error(
      'Error while validating course.',
      $q->errorInfo()
    );
  }
};

function startPlanCourse($planId, $courseId) {
  $dbh = DB::connect();

  $q = $dbh->prepare("
    UPDATE tr_plan_course
    SET started = SYSDATE,
        completed = NULL,
        validated_by = NULL,
        validation_date = NULL,
        expiration_date = NULL
    WHERE plan_id = :planId AND course_id = :courseId
  ");

  $q->bindParam(':planId', $planId, PDO::PARAM_INT, 10);
  $q->bindParam(':courseId', $courseId, PDO::PARAM_INT, 10);

  if ($q->execute()) {
    Http::send_ok("Course data has been updated.");
  } else {
    Http::send_error(
      'Error while validating course.',
      $q->errorInfo()
    );
  }
};

function completePlanCourse($planId, $courseId) {
  $dbh = DB::connect();
// TODO check if STARTED before completing
  $q = $dbh->prepare("
    UPDATE tr_plan_course
    SET completed = SYSDATE,
        validated_by = NULL,
        validation_date = NULL
    WHERE plan_id = :planId AND course_id = :courseId
  ");

  $q->bindParam(':planId', $planId, PDO::PARAM_INT, 10);
  $q->bindParam(':courseId', $courseId, PDO::PARAM_INT, 10);

  if ($q->execute()) {
    Http::send_ok("Course data has been updated.");
  } else {
    Http::send_error(
      'Error while validating course.',
      $q->errorInfo()
    );
  }
};

function validatePlanCourse($planId, $trainingId, $courseId) {

  $dbh = DB::connect();

  $q = $dbh->prepare("
    UPDATE tr_plan_course
    SET validated_by = :validated_by, validation_date = SYSDATE
    WHERE plan_id = :planId AND training_id = :trainingId AND course_id = :courseId
  ");

  $q->bindParam(':planId', $planId, PDO::PARAM_INT, 10);
  $q->bindParam(':trainingId', $trainingId, PDO::PARAM_INT, 10);
  $q->bindParam(':courseId', $courseId, PDO::PARAM_INT, 10);
  $q->bindParam(':validated_by', getUserName(), PDO::PARAM_STR, 20);

  if ($q->execute()) {
    Http::send_ok("Course data has been validated.");
  } else {
    Http::send_error(
      'Error while validating course.',
      $q->errorInfo()
    );
  }
}

function askValidation($planId, $courseId) {

  $dbh = DB::connect();

  $q = $dbh->prepare("
    SELECT  validation_by
    FROM tr_plan_course
    WHERE plan_id = :planId AND course_id = :courseId
  ");

  $q->bindParam(':planId', $planId, PDO::PARAM_INT, 10);
  $q->bindParam(':courseId', $courseId, PDO::PARAM_INT, 10);

  if ($q->execute()) {
    $to      = '';
    $subject = 'Training validation request';
    $headers = 'MIME-Version: 1.0 \r\n Content-type: text/html; charset=iso-8859-1 \r\n From: OPWT Training <no-reply@op-webtools.cern.ch>';

    $message = '
Dear Course Validator,

'.User::person(User::current())["display_name"].' completed his/her course and would like you to validate it.

Please follow this link to check the list of courses waiting for you validation.

https://op-webtools-test.web.cern.ch/training/frontend/#!/myclassroom

Thanks.';

    $validators = $q->fetch(PDO::FETCH_ASSOC)["VALIDATION_BY"];
    foreach (explode(',',$validators) as $k => $v) {
      $to .= "$v@cern.ch, ";
    }

    mail($to, $subject, $message, $headers);



    Http::send_ok("Course validation requested to $to.");
  } else {
    Http::send_error(
      'Error while retreiving course validators.',
      $q->errorInfo()
    );
  }

}

function deleteCourseFromPlans( $trainingId, $courseId) {

  $dbh = DB::connect();

  try {
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

      $q = $dbh->prepare("
      DELETE FROM tr_plan_course
      WHERE training_id = :trainingId AND course_id = :courseId
      ");
      $q->bindParam(':trainingId', $trainingId, PDO::PARAM_INT, 10);
      $q->bindParam(':courseId', $courseId, PDO::PARAM_INT, 10);

      $q->execute();

    Http::send_deleted();
  } catch (Exception $e) {
    $dbh->rollBack();
    Http::send_error(
      'Error while deleting courses from plan.',
      "Error in line : {$e->getLine()} | {$e->getMessage()}"
    );
  }
}

function deleteTrainingCoursesFromPlans( $trainingId) {

  $dbh = DB::connect();

  try {
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

      $q = $dbh->prepare("
      DELETE FROM tr_plan_course
      WHERE training_id = :trainingId
      ");
      $q->bindParam(':trainingId', $trainingId, PDO::PARAM_INT, 10);

      $q->execute();

    Http::send_deleted();
  } catch (Exception $e) {
    $dbh->rollBack();
    Http::send_error(
      'Error while deleting courses from plan.',
      "Error in line : {$e->getLine()} | {$e->getMessage()}"
    );
  }
}

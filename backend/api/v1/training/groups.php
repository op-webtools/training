<?php

// ROUTES --- --- ---

/**
 * @SWG\Get(
 *     path="/groups",
 *     summary="Get all groups",
 *     tags={"All", "Groups"},
 *     operationId="getGroups",
 *     produces={
 *         "application/json"
 *     },
 *     @SWG\Response(
 *         response=200,
 *         description="training courses.",
 *         @SWG\Schema(
 *             type="array",
 *             @SWG\Items(
 *                 type="object",
 *                 @SWG\Property(property="ID", type="integer"),
 *                 @SWG\Property(property="NAME", type="string")
 *                )
 *             )
 *         )
 *     )
 * )
 */
$route->get(
  '/groups',
  function() {
    http::send_json(getGroups());
  }
);

/**
 * @SWG\Put(
 *     path="/groups/{groupId}/trainings/order",
 *     summary="Set order of trainings in group",
 *     tags={"All", "Groups"},
 *     operationId="setTrainingsOrder",
 *     produces={
 *         "application/json"
 *     },
 *     @SWG\Parameter(
 *         in="path",
 *         name="groupId",
 *         required=true,
 *         type="integer"
 *     ),
 *     @SWG\Parameter(
 *         in="body",
 *         name="body",
 *         required=true,
 *         @SWG\Schema(
 *             type="array",
 *             @SWG\Items(
 *                 type="object",
 *                 @SWG\Property(property="ID", type="integer")
 *             )
 *         )
 *     ),
 *     @SWG\Response(
 *         response=200,
 *         description="Trainings have been sorted.",
 *     )
 * )
 */
$route->put(
  '/groups/:int/trainings/order',
  function($groupId, $params) {
    updateTrainingsOrder($groupId, $params);
  }
);

/**
 * @SWG\Post(
 *     path="/groups",
 *     summary="Create new group",
 *     tags={"All", "Groups"},
 *     operationId="createGroup",
 *     produces={
 *         "application/json"
 *     },
 *     @SWG\Parameter(
 *         in="body",
 *         name="body",
 *         required=true,
 *         schema=@SWG\Schema(
 *            @SWG\Property(property="group_name", type="string"),
 *         )
 *     ),
 *     @SWG\Response(
 *         response=200,
 *         description="Group has been created.",
 *         @SWG\Schema(
 *            @SWG\Property(property="ID", type="integer")
 *         )
 *     )
 * )
 */
$route->post(
  '/groups',
  function($params) {
    createGroup($params);
  }
);

/**
 * @SWG\Put(
 *     path="/groups/{groupId}",
 *     summary="Update groups",
 *     tags={"All", "Groups"},
 *     operationId="updateGroup",
 *     produces={
 *         "application/json"
 *     },
 *     @SWG\Parameter(
 *         in="body",
 *         name="body",
 *         required=true,
 *         schema=@SWG\Schema(
 *            @SWG\Property(property="groups", type="obejct"),
 *         )
 *     ),
 *     @SWG\Response(
 *         response=200,
 *         description="Group has been created.",
 *         @SWG\Schema(
 *            @SWG\Property(property="ID", type="integer")
 *         )
 *     )
 * )
 */
$route->put(
  '/groups/:int',
  function($groupID, $params) {
    if (Utils::isGroupAdmin($groupID)) {
      updateGroup($groupID, $params);
    } else {
      Utils::sendPrivligesErr();
    }
  }
);

/**
 * @SWG\Delete(
 *     path="/groups/{groupId}",
 *     summary="Delete Group",
 *     tags={"All", "Groups"},
 *     operationId="deleteGroup",
 *     produces={
 *         "application/json"
 *     },
 *     @SWG\Parameter(
 *         in="path",
 *         name="groupId",
 *         required=true,
 *         type="integer"
 *     ),
 *     @SWG\Response(
 *         response=204,
 *         description="Successfully deleted group."
 *     )
 * )
 */
$route->delete(
  '/groups/:int',
  function($groupID) {
    if (Utils::isGroupAdmin($groupID)) {
      deleteGroup($groupID);
    } else {
      Utils::sendPrivligesErr();
    }
  }
);


function getGroups(){

    $dbh = DB::connect();

    $q = $dbh->prepare("
      SELECT id, group_name, admin
      FROM tr_group
      ORDER BY group_name
    ");

    if ($q->execute()) {
      $groups = $q->fetchAll(PDO::FETCH_ASSOC);
      foreach ($groups as &$group) {
        $group["trainings"] = getGroupTrainings($group["ID"]);
      }
      return Utils::array_change_key_case_recursive($groups);
    } else {
      Http::send_error(
        'Error while getting groups.',
        $q->errorInfo()
      );
    }
}

function getGroupTrainings($groupId){

 $dbh = DB::connect();

  $q = $dbh->prepare("
    SELECT training_id as id, position
    FROM tr_training_group
    WHERE group_id = :groupId
  ");

  $q->bindParam(':groupId', $groupId, PDO::PARAM_INT, 10);

  if ($q->execute()) {
    return Utils::array_change_key_case_recursive($q->fetchAll(PDO::FETCH_ASSOC));
  } else {
    Http::send_error(
      'Error while getting group training.',
      $q->errorInfo()
    );
  }

}

function updateTrainingsOrder($groupId, $params) {

  $dbh = DB::connect();

  $pos = 1;
  $query = 'UPDATE tr_training_group SET position = :pos WHERE group_id = :groupId AND training_id = :trainingId';
  $q = $dbh->prepare($query);
  $q->bindParam(':groupId', $groupId, PDO::PARAM_INT, 10);
  foreach ($params["trainings"] as $training) {
    $q->bindParam(':trainingId', $training['id'], PDO::PARAM_INT, 10);
    $q->bindParam(':pos', $training['position'], PDO::PARAM_INT, 10);
    if (!$q->execute()) {
      Http::send_error(
        'Error while updating training order.',
        $q->errorInfo()
      );
    }
    $pos++;
  }
  Http::send_ok('order saved');

}

function createGroup($params){

  $dbh = DB::connect();

  $group_name = $params["group_name"]?: Http::send_error("You need to provide a group name.");

  $q = $dbh->prepare("
    INSERT INTO tr_group (group_name, admin)
    VALUES (:group_name, :admin)
    RETURNING id INTO :groupID
  ");
  $admin = User::current();
  $q->bindParam(':group_name', $group_name, PDO::PARAM_STR, 50);
  $q->bindParam(':admin', $admin, PDO::PARAM_STR, 50);
  $q->bindParam(':groupID', $groupID, PDO::PARAM_INT, 10);

  if ($q->execute()) {
    Http::send_created(array('id' => $groupID ));
  } else {
    Http::send_error(
      'Error while creating new group.',
      $q->errorInfo()
    );
  }
}

function updateGroup($groupID, $params) {
    $dbh = DB::connect();

    $q = $dbh->prepare("
      UPDATE tr_group
      SET group_name = :group_name
      WHERE id = :groupID
    ");

    $groupName = Utils::remove_accents($params['group_name']);
    $q->bindParam(':group_name', $groupName, PDO::PARAM_STR);
    $q->bindParam(':groupID', $groupID, PDO::PARAM_INT, 10);

    if ($q->execute()) {
      Http::send_ok();
    } else {
      Http::send_error(
        'Error while updating group.',
        $q->errorInfo()
      );
    };
}

function deleteGroup($groupID) {

  $dbh = DB::connect();

  try {
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $dbh->beginTransaction();

    $q = $dbh->prepare("DELETE FROM tr_training_group WHERE group_id = :groupID");
    $q->bindParam(':groupID', $groupID, PDO::PARAM_INT, 10);

    $q->execute();

    $q2 = $dbh->prepare("DELETE FROM tr_group WHERE id = :groupID");
    $q2->bindParam(':groupID', $groupID, PDO::PARAM_INT, 10);

    $q2->execute();

    $dbh->commit();
  } catch (Exception $e) {
    $dbh->rollBack();
    Http::send_error(
      'Error while deleting group.',
      $q->errorInfo()
    );
  }

}

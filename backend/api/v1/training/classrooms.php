<?php

/**
 * @SWG\Get(
 *     path="/classrooms/",
 *     summary="Get all classroom ",
 *     tags={"All", "Classrooms"},
 *     operationId="getClassroom",
 *     produces={
 *         "application/json"
 *     },
 *     @SWG\Response(
 *         response=200,
 *         description="List of Classrooms.",
 *         @SWG\Schema(
 *             type="array",
 *             @SWG\Item(type="string")
 *         )
 *     )
 * )
 */
$route->get(
  '/classrooms',
  function() {
    Http::send_json(getAllClassroom());
  }
);

/**
 * @SWG\Get(
 *     path="/classrooms/{classroomId}",
 *     summary="Get classroom ",
 *     tags={"All", "Classrooms"},
 *     operationId="getClassroom",
 *     produces={
 *         "application/json"
 *     },
 *     @SWG\Parameter(
 *         in="path",
 *         name="classroomId",
 *         required=true,
 *         type="integer"
 *     ),
 *     @SWG\Response(
 *         response=200,
 *         description="Classroom user courses.",
 *         @SWG\Schema(
 *             type="array",
 *             @SWG\Item(type="string")
 *         )
 *     )
 * )
 */
$route->get(
  '/classrooms/:int',
  function($classroomId) {
    Http::send_json(getClassroom($classroomId));
  }
);

/**
 * @SWG\Put(
 *     path="/classrooms/{classroomId}/name",
 *     summary="Update classroom name",
 *     tags={"All", "Classroom"},
 *     operationId="updateClassroomName",
 *     produces={
 *         "application/json"
 *     },
 *     @SWG\Parameter(
 *         in="body",
 *         name="body",
 *         required=true,
 *         schema=@SWG\Schema(
 *            @SWG\Property(property="name", type="string"),
 *         )
 *     ),
 *     @SWG\Response(
 *         response=200,
 *         description="Name has been changed.",
 *         )
 *     )
 * )
 */
$route->put(
  '/classrooms/:int/name',
  function($classroomId, $params) {
    updateClassroomName($classroomId, $params);
  }
);

/**
 * @SWG\Get(
 *     path="/classrooms/{classroomId}/visitors",
 *     summary="Get classroom visitors",
 *     tags={"All", "Classrooms"},
 *     operationId="getClassroomVisitors",
 *     produces={
 *         "application/json"
 *     },
 *     @SWG\Parameter(
 *         in="path",
 *         name="classroomId",
 *         required=true,
 *         type="integer"
 *     ),
 *     @SWG\Response(
 *         response=200,
 *         description="Classroom visitors.",
 *         @SWG\Schema(
 *             type="array",
 *             @SWG\Item(type="string")
 *         )
 *     )
 * )
 */
$route->get(
  '/classrooms/:int/visitors',
  function($classroomId) {
    getClassroomVisitors($classroomId);
  }
);

/**
 * @SWG\Get(
 *     path="/users/{userId}/classrooms",
 *     summary="Get user's classroom ",
 *     tags={"All", "Classrooms"},
 *     operationId="getUsersClassrooms",
 *     produces={
 *         "application/json"
 *     },
 *     @SWG\Parameter(
 *         in="path",
 *         name="userId",
 *         required=true,
 *         type="string"
 *     ),
 *     @SWG\Response(
 *         response=200,
 *         description="Classroom for User.",
 *         @SWG\Schema(
 *             type="array",
 *             @SWG\Item(type="string")
 *         )
 *     )
 * )
 */
$route->get(
  '/users/:string/classrooms',
  function($userId) {
    Http::send_json(getUsersClassrooms($userId));
  }
);


/**
 * @SWG\Post(
 *     path="/classrooms/{classroomId}/visitors",
 *     summary="Add classroom visitor",
 *     tags={"All", "Classrooms"},
 *     operationId="addClassroomVisitor",
 *     produces={
 *         "application/json"
 *     },
 *     @SWG\Parameter(
 *         in="path",
 *         name="classroomId",
 *         required=true,
 *         type="integer"
 *     ),
 *     @SWG\Parameter(
 *         in="body",
 *         name="body",
 *         required=true,
 *         @SWG\Schema(
 *             type="object",
 *             required={"user_id"},
 *             @SWG\Property(property="user_id", type="string", example="phardyn")
 *         )
 *     ),
 *     @SWG\Response(
 *         response=200,
 *         description="Classroom visitor have been added.",
 *     )
 * )
 */
$route->post(
  '/classrooms/:int/visitors',
  function($classroomId, $params) {
    Http::send_created(addClassroomVisitor($classroomId, $params));
  }
);

/**
 * @SWG\Delete(
 *     path="/classrooms/{classroomId}/visitors/{visitorId}",
 *     summary="Delete classroom visitor",
 *     tags={"All", "Classrooms"},
 *     operationId="deleteClassroomVisitor",
 *     produces={
 *         "application/json"
 *     },
 *     @SWG\Parameter(
 *         in="path",
 *         name="classroomId",
 *         required=true,
 *         type="integer"
 *     ),
 *     @SWG\Parameter(
 *         in="path",
 *         name="visitorId",
 *         required=true,
 *         type="string"
 *     ),
 *     @SWG\Response(
 *         response=200,
 *         description="Classroom visitor have been deleted.",
 *     )
 * )
 */
$route->delete(
  '/classrooms/:int/visitors/:string',
  function($classroomId, $visitorId) {
    Http::send_deleted(deleteClassroomVisitor($classroomId, $visitorId));
  }
);

/**
 * @SWG\Post(
 *     path="/classrooms/{classroomId}/users/{userId}",
 *     summary="Add classroom user",
 *     tags={"All", "Classrooms"},
 *     operationId="addClassroomUser",
 *     produces={
 *         "application/json"
 *     },
 *     @SWG\Parameter(
 *         in="path",
 *         name="classroomId",
 *         required=true,
 *         type="integer"
 *     ),
 *     @SWG\Parameter(
 *         in="path",
 *         name="userId",
 *         required=true,
 *         type="string"
 *     ),
 *     @SWG\Response(
 *         response=200,
 *         description="Classroom user have been added.",
 *     )
 * )
 */
$route->post(
  '/classrooms/:int/users/:string',
  function($classroomId, $userId) {
    Http::send_created(addClassroomUser($classroomId, $userId));
  }
);

/**
 * @SWG\Delete(
 *     path="/classrooms/{classroomId}/users/{userId}",
 *     summary="Delete classroom user",
 *     tags={"All", "Classrooms"},
 *     operationId="deleteClassroomUser",
 *     produces={
 *         "application/json"
 *     },
 *     @SWG\Parameter(
 *         in="path",
 *         name="classroomId",
 *         required=true,
 *         type="integer"
 *     ),
 *     @SWG\Parameter(
 *         in="path",
 *         name="userId",
 *         required=true,
 *         type="string"
 *     ),
 *     @SWG\Response(
 *         response=200,
 *         description="Classroom user have been deleted.",
 *     )
 * )
 */
$route->delete(
  '/classrooms/:int/users/:string',
  function($classroomId, $userId) {
    Http::send_deleted(deleteClassroomUser($classroomId, $userId));
  }
);

/**
 * @SWG\Get(
 *     path="/classrooms/{classroomId}/users",
 *     summary="Get classroom visitors",
 *     tags={"All", "Classrooms"},
 *     operationId="getClassroomUsers",
 *     produces={
 *         "application/json"
 *     },
 *     @SWG\Parameter(
 *         in="path",
 *         name="classroomId",
 *         required=true,
 *         type="integer"
 *     ),
 *     @SWG\Response(
 *         response=200,
 *         description="Classroom visitors.",
 *         @SWG\Schema(
 *             type="array",
 *             @SWG\Item(type="string")
 *         )
 *     )
 * )
 */
$route->get(
  '/classrooms/:int/users',
  function($classroomId) {
    Http::send_json(getClassroomUsers($classroomId));
  }
);

/**
 * @SWG\Post(
 *     path="/classrooms/{classroomId}/users/{userId}/trainings/{trainingId}/courses/{courseId}",
 *     summary="Add course to classroom user",
 *     tags={"All", "Classrooms"},
 *     operationId="addClassroomUserCourse",
 *     produces={
 *         "application/json"
 *     },
 *     @SWG\Parameter(
 *         in="path",
 *         name="classroomId",
 *         required=true,
 *         type="integer"
 *     ),
 *     @SWG\Parameter(
 *         in="path",
 *         name="userId",
 *         required=true,
 *         type="string"
 *     ),
 *     @SWG\Parameter(
 *         in="path",
 *         name="trainingId",
 *         required=true,
 *         type="integer"
 *     ),
 *     @SWG\Parameter(
 *         in="path",
 *         name="courseId",
 *         required=true,
 *         type="integer"
 *     ),
 *     @SWG\Response(
 *         response=200,
 *         description="Course have been added too classroom user.",
 *     )
 * )
 */
$route->post(
  '/classrooms/:int/users/:string/trainings/:int/courses/:int',
  function($classroomId, $userId, $trainingId, $courseId) {
    Http::send_created(addClassroomUserCourse($classroomId, $userId, $trainingId, $courseId));
  }
);

/**
 * @SWG\Post(
 *     path="/classrooms/{classroomId}/users/{userId}/trainings/{trainingId}/",
 *     summary="Add  all training courses to classroom user",
 *     tags={"All", "Classrooms"},
 *     operationId="addClassroomUserTraining",
 *     produces={
 *         "application/json"
 *     },
 *     @SWG\Parameter(
 *         in="path",
 *         name="classroomId",
 *         required=true,
 *         type="integer"
 *     ),
 *     @SWG\Parameter(
 *         in="path",
 *         name="userId",
 *         required=true,
 *         type="string"
 *     ),
 *     @SWG\Parameter(
 *         in="path",
 *         name="trainingId",
 *         required=true,
 *         type="integer"
 *     ),
 *     @SWG\Response(
 *         response=200,
 *         description="Training has been added too classroom user.",
 *     )
 * )
 */
$route->post(
  '/classrooms/:int/users/:string/trainings/:int/',
  function($classroomId, $userId, $trainingId) {
    Http::send_created(addClassroomUserTraining($classroomId, $userId, $trainingId));
  }
);

/**
 * @SWG\Put(
 *     path="/classrooms/{classroomId}/users/{userId}/trainings/{trainingId}/courses/{edmsId}/deadline",
 *     summary="Update deadline of classroom user course",
 *     tags={"All", "Classrooms"},
 *     operationId="updateClassroomUserCourseDeadline",
 *     produces={
 *         "application/json"
 *     },
 *     @SWG\Parameter(
 *         in="path",
 *         name="classroomId",
 *         required=true,
 *         type="integer"
 *     ),
 *     @SWG\Parameter(
 *         in="path",
 *         name="userId",
 *         required=true,
 *         type="string"
 *     ),
 *     @SWG\Parameter(
 *         in="path",
 *         name="edmsId",
 *         required=true,
 *         type="integer"
 *     ),
 *     @SWG\Parameter(
 *         in="body",
 *         name="body",
 *         required=true,
 *         @SWG\Schema(
 *             type="object",
 *             required={"course_edms"},
 *             @SWG\Property(property="deadline", type="string"),
 *         )
 *     ),
 *     @SWG\Response(
 *         response=200,
 *         description="Deadline have been updated.",
 *     )
 * )
 */
$route->put(
  '/classrooms/:int/users/:string/trainings/:int/courses/:int/deadline',
  function($classroomId, $userId, $trainingId, $edmsId, $params) {
    Http::send_created(updateClassroomUserCourseDeadline($classroomId, $userId, $trainingId, $edmsId, $params));
  }
);

/**
 * @SWG\Put(
 *     path="/classrooms/{classroomId}/users/{userId}/trainings/{trainingId}/courses/{edmsId}/responsible",
 *     summary="Update responsible of classroom user course",
 *     tags={"All", "Classrooms"},
 *     operationId="updateClassroomUserCourseResponsible",
 *     produces={
 *         "application/json"
 *     },
 *     @SWG\Parameter(
 *         in="path",
 *         name="classroomId",
 *         required=true,
 *         type="integer"
 *     ),
 *     @SWG\Parameter(
 *         in="path",
 *         name="userId",
 *         required=true,
 *         type="string"
 *     ),
 *     @SWG\Parameter(
 *         in="path",
 *         name="edmsId",
 *         required=true,
 *         type="integer"
 *     ),
 *     @SWG\Parameter(
 *         in="body",
 *         name="body",
 *         required=true,
 *         @SWG\Schema(
 *             type="object",
 *             required={"course_edms"},
 *             @SWG\Property(property="responsible", type="string"),
 *         )
 *     ),
 *     @SWG\Response(
 *         response=200,
 *         description="Responsible have been updated.",
 *     )
 * )
 */
$route->put(
  '/classrooms/:int/users/:string/trainings/:int/courses/:int/responsible',
  function($classroomId, $userId, $trainingId, $edmsId, $params) {
    Http::send_created(updateClassroomUserCourseResponsible($classroomId, $userId, $trainingId, $edmsId, $params));
  }
);

/**
 * @SWG\Put(
 *     path="/classrooms/{classroomId}/users/{userId}/trainings/order",
 *     summary="Update order of classroom user trainings",
 *     tags={"All", "Classrooms"},
 *     operationId="updateClassroomUserTrainingOrder",
 *     produces={
 *         "application/json"
 *     },
 *     @SWG\Parameter(
 *         in="path",
 *         name="classroomId",
 *         required=true,
 *         type="integer"
 *     ),
 *     @SWG\Parameter(
 *         in="path",
 *         name="userId",
 *         required=true,
 *         type="string"
 *     ),
 *     @SWG\Parameter(
 *         in="body",
 *         name="body",
 *         required=true,
 *         @SWG\Schema(
 *             type="array",
 *             required={"course_edms"},
 *             @SWG\Property(property="responsible", type="integer"),
 *         )
 *     ),
 *     @SWG\Response(
 *         response=200,
 *         description="Trainings order has been updated.",
 *     )
 * )
 */
$route->put(
  '/classrooms/:int/users/:string/trainings/order',
  function($classroomId, $userId, $params) {
    Http::send_created(updateClassroomUserTrainingOrder($classroomId, $userId, $params));
  }
);

/**
 * @SWG\Delete(
 *     path="/classrooms/{classroomId}/users/{userId}/training/{trainingId}/courses/{edmsId}",
 *     summary="Delete classroom user",
 *     tags={"All", "Classrooms"},
 *     operationId="deleteClassroomUser",
 *     produces={
 *         "application/json"
 *     },
 *     @SWG\Parameter(
 *         in="path",
 *         name="classroomId",
 *         required=true,
 *         type="integer"
 *     ),
 *     @SWG\Parameter(
 *         in="path",
 *         name="userId",
 *         required=true,
 *         type="string"
 *     ),
 *     @SWG\Parameter(
 *         in="path",
 *         name="trainingId",
 *         required=true,
 *         type="integer"
 *     ),
 *     @SWG\Parameter(
 *         in="path",
 *         name="edmsId",
 *         required=true,
 *         type="integer"
 *     ),
 *     @SWG\Response(
 *         response=200,
 *         description="Classroom user have been deleted.",
 *     )
 * )
 */
$route->delete(
  '/classrooms/:int/users/:string/trainings/:int/courses/:int',
  function($classroomId, $userId, $trainingId, $edmsId) {
    Http::send_deleted(deleteClassroomUserCourse($classroomId, $userId, $trainingId, $edmsId));
  }
);

/**
 * @SWG\Delete(
 *     path="/classrooms/{classroomId}/users/{userId}/trainings/{trainingId}",
 *     summary="Delete classroom user",
 *     tags={"All", "Classrooms"},
 *     operationId="deleteClassroomUser",
 *     produces={
 *         "application/json"
 *     },
 *     @SWG\Parameter(
 *         in="path",
 *         name="classroomId",
 *         required=true,
 *         type="integer"
 *     ),
 *     @SWG\Parameter(
 *         in="path",
 *         name="userId",
 *         required=true,
 *         type="string"
 *     ),
 *     @SWG\Parameter(
 *         in="path",
 *         name="trainingId",
 *         required=true,
 *         type="integer"
 *     ),
 *     @SWG\Parameter(
 *         in="path",
 *         name="edmsId",
 *         required=true,
 *         type="integer"
 *     ),
 *     @SWG\Response(
 *         response=200,
 *         description="Classroom user have been deleted.",
 *     )
 * )
 */
$route->delete(
  '/classrooms/:int/users/:string/trainings/:int/',
  function($classroomId, $userId, $trainingId, $edmsId) {
    Http::send_deleted(deleteClassroomUserTrainingCourses($classroomId, $userId, $trainingId));
  }
);

function updateClassroomName($classroomId, $params) {
    if (!Utils::isClassroomAdmin($classroomId)) 
      Utils::sendPrivligesErr();

    $dbh = DB::connect();

    $q = $dbh->prepare("
      UPDATE tr_classroom
      SET name = :name
      WHERE id = :classroomId
    ");

    $name = Utils::remove_accents($params['name']);
    $q->bindParam(':name', $name, PDO::PARAM_STR);
    $q->bindParam(':classroomId', $classroomId, PDO::PARAM_INT, 10);

    if ($q->execute()) {
      Http::send_ok();
    } else {
      Http::send_error(
        'Error while updating classroom name.',
        $q->errorInfo()
      );
    };
}

function getClassroomVisitors($classroomId){
  if (!Utils::isClassroomAdmin($classroomId)) 
      Utils::sendPrivligesErr();

  $dbh = DB::connect();

  $q = $dbh->prepare("
    SELECT  user_id
    FROM tr_classroom_visitor
    WHERE classroom_id = :classroomId
  ");
  $q->bindParam(':classroomId', $classroomId, PDO::PARAM_INT, 10);

  if ($q->execute()) {
    Http::send_json(Utils::array_change_key_case_recursive($q->fetchAll(PDO::FETCH_ASSOC)));
  } else {
    Http::send_error(
      'Error while getting classroom visitors.',
      $q->errorInfo()
    );
  }
};

function addClassroomVisitor($classroomId, $params){
  if (!Utils::isClassroomAdmin($classroomId)) 
      Utils::sendPrivligesErr();

  $dbh = DB::connect();

  $q = $dbh->prepare("
    INSERT INTO tr_classroom_visitor (classroom_id, user_id)
    VALUES (:classroomId, :userId)
  ");

  $q->bindParam(':classroomId', $classroomId, PDO::PARAM_INT, 10);
  $q->bindParam(':userId', $params['user_id'], PDO::PARAM_STR);

  if (!$q->execute()) {
    Http::send_error(
      'Error while adding classroom visitor.',
      $q->errorInfo()
    );
  }
};

function addClassroomUser($classroomId, $userId){
  if (!Utils::isClassroomAdmin($classroomId)) 
      Utils::sendPrivligesErr();

  $dbh = DB::connect();

  $q = $dbh->prepare("
    INSERT INTO tr_classroom_user (classroom_id, user_id)
    VALUES (:classroomId, :userId)
  ");

  $q->bindParam(':classroomId', $classroomId, PDO::PARAM_INT, 10);
  $q->bindParam(':userId', $userId, PDO::PARAM_STR);

  if (!$q->execute()) {
    Http::send_error(
      'Error while adding classroom user.',
      $q->errorInfo()
    );
  }
};

function deleteClassroomVisitor($classroomId, $visitorId){
  if (!Utils::isClassroomAdmin($classroomId)) 
      Utils::sendPrivligesErr();

  $dbh = DB::connect();

  $q = $dbh->prepare("
    DELETE FROM tr_classroom_visitor
    WHERE classroom_id = :classroomId
    AND user_id = :visitorId
  ");

  $q->bindParam(':classroomId', $classroomId, PDO::PARAM_INT, 10);
  $q->bindParam(':visitorId', $visitorId, PDO::PARAM_STR);

  if (!$q->execute()) {
    Http::send_error(
      'Error while deleting classroom visitor.',
      $q->errorInfo()
    );
  }
};

function deleteClassroomUser($classroomId, $userId){
  if (!Utils::isClassroomAdmin($classroomId)) 
      Utils::sendPrivligesErr();

  $dbh = DB::connect();

  try {
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $dbh->beginTransaction();

    $q = $dbh->prepare("
      DELETE FROM tr_classroom_user_course
      WHERE classroom_id = :classroomId
      AND user_id = :userId
    ");
    $q->bindParam(':classroomId', $classroomId, PDO::PARAM_INT, 10);
    $q->bindParam(':userId', $userId, PDO::PARAM_STR);

    $q->execute();

    $q2 = $dbh->prepare("
      DELETE FROM tr_classroom_user
      WHERE classroom_id = :classroomId
      AND user_id = :userId
    ");

    $q2->bindParam(':classroomId', $classroomId, PDO::PARAM_INT, 10);
    $q2->bindParam(':userId', $userId, PDO::PARAM_STR);

    $q2->execute();

    $dbh->commit();
  } catch (Exception $e) {
    $dbh->rollBack();
    Http::send_error(
      'Error while deleting classroom user.',
      $q->errorInfo()
    );
  }
};

function addClassroomUserCourse($classroomId, $userId, $trainingId, $courseId){
  if (!Utils::isClassroomAdmin($classroomId)) 
      Utils::sendPrivligesErr();

  $dbh = DB::connect();

  $q = $dbh->prepare("
    INSERT INTO tr_classroom_user_course (classroom_id, user_id, training_id, course_edms)
    VALUES (:classroomId, :userId, :trainingId, :edmsId)
  ");

  $q->bindParam(':classroomId', $classroomId, PDO::PARAM_INT, 10);
  $q->bindParam(':userId', $userId, PDO::PARAM_STR);
  $q->bindParam(':trainingId', $trainingId, PDO::PARAM_INT, 10);
  $q->bindParam(':edmsId', $courseId, PDO::PARAM_INT, 10);

  if (!$q->execute()) {
    Http::send_error(
      'Error while adding classroom user course.',
      $q->errorInfo()
    );
  }
};

function updateClassroomUserCourseDeadline($classroomId, $userId, $trainingId, $edmsId, $params){
  if (!Utils::isClassroomAdmin($classroomId)) 
      Utils::sendPrivligesErr();

  $dbh = DB::connect();

  $q = $dbh->prepare("
    UPDATE tr_classroom_user_course
    SET deadline = TO_DATE(:deadline, 'yyyy-mm-dd')
    WHERE classroom_id = :classroomId
    AND user_id = :userId
    AND training_id = :trainingId
    AND course_edms = :edmsId
  ");

  $q->bindParam(':deadline', $params['deadline'], PDO::PARAM_STR, 50);
  $q->bindParam(':classroomId', $classroomId, PDO::PARAM_INT, 10);
  $q->bindParam(':userId', $userId, PDO::PARAM_STR);
  $q->bindParam(':trainingId', $trainingId, PDO::PARAM_INT, 10);
  $q->bindParam(':edmsId', $edmsId, PDO::PARAM_INT, 10);

  if (!$q->execute()) {
    Http::send_error(
      'Error while updating deadline.',
      $q->errorInfo()
    );
  }
};

function updateClassroomUserCourseResponsible($classroomId, $userId, $trainingId, $edmsId, $params){
  if (!Utils::isClassroomAdmin($classroomId)) 
      Utils::sendPrivligesErr();

  $dbh = DB::connect();

  $q = $dbh->prepare("
    UPDATE tr_classroom_user_course
    SET responsible = :responsible
    WHERE classroom_id = :classroomId
    AND user_id = :userId
    AND training_id = :trainingId
    AND course_edms = :edmsId
  ");

  $q->bindParam(':responsible', $params['responsible'], PDO::PARAM_STR, 200);
  $q->bindParam(':classroomId', $classroomId, PDO::PARAM_INT, 10);
  $q->bindParam(':userId', $userId, PDO::PARAM_STR);
  $q->bindParam(':trainingId', $trainingId, PDO::PARAM_INT, 10);
  $q->bindParam(':edmsId', $edmsId, PDO::PARAM_INT, 10);

  if (!$q->execute()) {
    Http::send_error(
      'Error while updating responsible.',
      $q->errorInfo()
    );
  }
};

function deleteClassroomUserCourse($classroomId, $userId, $trainingId, $edmsId){
  if (!Utils::isClassroomAdmin($classroomId)) 
      Utils::sendPrivligesErr();

  $dbh = DB::connect();

  $q = $dbh->prepare("
    DELETE FROM tr_classroom_user_course
    WHERE classroom_id = :classroomId
    AND user_id = :userId
    AND training_id = :trainingId
    AND course_edms = :edmsId
  ");

  $q->bindParam(':classroomId', $classroomId, PDO::PARAM_INT, 10);
  $q->bindParam(':userId', $userId, PDO::PARAM_STR);
  $q->bindParam(':trainingId', $trainingId, PDO::PARAM_INT, 10);
  $q->bindParam(':edmsId', $edmsId, PDO::PARAM_INT, 10);

  if (!$q->execute()) {
    Http::send_error(
      'Error while deleting classroom user course.',
      $q->errorInfo()
    );
  }
};

function deleteClassroomUserTrainingCourses($classroomId, $userId, $trainingId){
  if (!Utils::isClassroomAdmin($classroomId)) 
      Utils::sendPrivligesErr();

  $dbh = DB::connect();

  $q = $dbh->prepare("
    DELETE FROM tr_classroom_user_course
    WHERE classroom_id = :classroomId
    AND user_id = :userId
    AND training_id = :trainingId
  ");

  $q->bindParam(':classroomId', $classroomId, PDO::PARAM_INT, 10);
  $q->bindParam(':userId', $userId, PDO::PARAM_STR);
  $q->bindParam(':trainingId', $trainingId, PDO::PARAM_INT, 10);

  if (!$q->execute()) {
    Http::send_error(
      'Error while deleting classroom user courses.',
      $q->errorInfo()
    );
  }
};

function getId($res) {
  return $res;
}

function getClassroomUsers($classroomId){
  // if (!(Utils::isClassroomAdmin($classroomId) || Utils::isClassroomVisitor($classroomId))) 
  //     Utils::sendPrivligesErr();

  $dbh = DB::connect();

  $q = $dbh->prepare("
    SELECT  user_id
    FROM tr_classroom_user
    WHERE classroom_id = :classroomId
  ");
  $q->bindParam(':classroomId', $classroomId, PDO::PARAM_INT, 10);

  if ($q->execute()) {
    return (Utils::array_change_key_case_recursive($q->fetchAll(PDO::FETCH_FUNC, "getId")));
  } else {
    Http::send_error(
      'Error while getting classroom users.',
      $q->errorInfo()
    );
  }
};

function getAllClassroom() {
  $dbh = DB::connect();

  $q = $dbh->prepare("
    SELECT id, name
    FROM tr_classroom
  ");

  if ($q->execute()) {
    $tmp = Utils::array_change_key_case_recursive($q->fetchAll(PDO::FETCH_ASSOC));
    foreach ($tmp as &$t) {
      $t["users"] = getClassroomUsers($t["id"]);
    }
    return $tmp;
  } else {
    Http::send_error(
      'Error while getting users classrooms.',
      $q->errorInfo()
    );
  } 
}

function getUsersClassrooms($userId) {
  $dbh = DB::connect();

  $q = $dbh->prepare("
    select distinct classroom_id from (
    select  classroom_id
    from  tr_classroom_user  where user_id = :userId
    union all
    select classroom_id
    from  tr_classroom_admin where admin = :userId)
  ");
  $q->bindParam(':userId', $userId, PDO::PARAM_STR, 10);

  if ($q->execute()) {
    return Utils::array_change_key_case_recursive($q->fetchAll(PDO::FETCH_FUNC, "getId"));
  } else {
    Http::send_error(
      'Error while getting users classrooms.',
      $q->errorInfo()
    );
  } 
}

function getClassroomAdmins($classroomId){
  $dbh = DB::connect();

  $q = $dbh->prepare("
    SELECT admin
    FROM tr_classroom_admin
    WHERE CLASSROOM_ID = :classroomId
  ");
  $userID = User::current();
  $q->bindParam(':classroomId', $classroomId, PDO::PARAM_INT, 20);

  if ($q->execute()) {
    return (Utils::array_change_key_case_recursive($q->fetchAll(PDO::FETCH_FUNC, "getId")));
  } else {
    Http::send_error(
      'Error while getting classroom admins.',
      $q->errorInfo()
    );
  }
}

function getClassroom($classroomId) {
  $dbh = DB::connect();

  $q = $dbh->prepare("
    SELECT id, name
    FROM tr_classroom
    where id = :classroomId
  ");
  $q->bindParam(':classroomId', $classroomId, PDO::PARAM_INT, 10);

  if ($q->execute()) {
    $classroom = $q->fetch(PDO::FETCH_ASSOC);
    $classroom['users'] = getClassroomUsers($classroomId);
    $classroom['admins'] = getClassroomAdmins($classroomId);
    return Utils::array_change_key_case_recursive($classroom);
  } else {
    Http::send_error(
      'Error while getting classrooms.',
      $q->errorInfo()
    );
  } 
}

function addClassroomUserTraining($classroomId, $userId, $trainingId) {
  $dbh = DB::connect();

  if (!Utils::isClassroomAdmin($classroomId)) 
      Utils::sendPrivligesErr();

  try {
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $dbh->beginTransaction();

    $q = $dbh->prepare("
      SELECT  course_edms
      FROM tr_training_course
      WHERE training_id = :trainingId
      AND course_edms not in (
        SELECT course_edms from tr_classroom_user_course 
        where tr_classroom_user_course.classroom_id = :classroomId 
        and tr_classroom_user_course.user_id = :userId
        and tr_training_course.training_id = :trainingId
      )
    ");
    $q->bindParam(':trainingId', $trainingId, PDO::PARAM_INT, 10);
    $q->bindParam(':userId', $userId, PDO::PARAM_STR);
    $q->bindParam(':classroomId', $classroomId, PDO::PARAM_INT, 10);

    if ($q->execute()) {
      $res = $q->fetchAll(PDO::FETCH_FUNC, "getId");
    } else {
      Http::send_error(
        'Error while getting training courses.',
        $q->errorInfo()
      );
    }
    if (count($res)) {
      $query = "INSERT ALL ";
      foreach ($res as $tr) {
        $query .= " INTO tr_classroom_user_course (classroom_id, user_id, training_id, course_edms) VALUES (:classroomId, :userId, :trainingId, $tr)";
      }
  
      $query .= "SELECT * FROM dual";
      $q2 = $dbh->prepare($query);
  
      $q2->bindParam(':classroomId', $classroomId, PDO::PARAM_INT, 10);
      $q2->bindParam(':trainingId', $trainingId, PDO::PARAM_INT, 10);
      $q2->bindParam(':userId', $userId, PDO::PARAM_STR);
  
      $q2->execute();
  
      $dbh->commit();
    }
  } catch (Exception $e) {
    $dbh->rollBack();
    Http::send_error(
      'Error while adding classroom user training.',
      $q->errorInfo()
    );
  }
}

function updateClassroomUserTrainingOrder($classroomId, $userId, $params) {
  $dbh = DB::connect();

  if (!Utils::isClassroomAdmin($classroomId)) 
      Utils::sendPrivligesErr();

  // Wrap in a transaction

  $pos = 1;
  $query = 'UPDATE tr_classroom_user_course SET training_pos = :pos 
    WHERE classroom_id = :classroomId 
      AND user_id = :userId
      AND training_id = :trainingId';
  $q = $dbh->prepare($query);
  $q->bindParam(':classroomId', $classroomId, PDO::PARAM_INT, 10);
  $q->bindParam(':userId', $userId, PDO::PARAM_STR, 20);
  foreach ($params["trainings"] as $training) {
    $q->bindParam(':trainingId', $training, PDO::PARAM_INT, 10);
    $q->bindParam(':pos', $pos, PDO::PARAM_INT, 10);
    if (!$q->execute()) {
      Http::send_error(
        'Error while updating training order.',
        $q->errorInfo()
      );
    }
    $pos++;
  }
  Http::send_ok('order saved');
}
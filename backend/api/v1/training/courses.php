<?php

// ROUTES --- --- ---

/**
 * @SWG\Get(
 *     path="/courses",
 *     summary="Get list of courses",
 *     tags={"All", "Courses"},
 *     operationId="getAllCourses",
 *     produces={
 *         "application/json"
 *     },
 *     @SWG\Response(
 *         response=200,
 *         description="List of courses.",
 *         @SWG\Schema(
 *             type="array",
 *             @SWG\Items(
 *                 type="object",
 *                 @SWG\Property(property="ID", type="integer"),
 *                 @SWG\Property(property="TITLE", type="string"),
 *                 @SWG\Property(property="DESCRIPTION", type="string"),
 *                 @SWG\Property(property="EDMS", type="integer"),
 *                 @SWG\Property(property="RESPONSIBLE", type="string"),
 *                 @SWG\Property(property="CREATED", type="string"),
 *                 @SWG\Property(property="UPDATED", type="string"),
 *                 @SWG\Property(property="TRAININGS", type="integer")
 *             )
 *         )
 *     )
 * )
 */
$route->get(
  '/courses',
  function() {
    getAllCourses();
  }
);

/**
 * @SWG\Get(
 *     path="/courses/{courseId}",
 *     summary="Get course data",
 *     tags={"All", "Courses"},
 *     operationId="getCourse",
 *     produces={
 *         "application/json"
 *     },
 *     @SWG\Parameter(
 *         in="path",
 *         name="courseId",
 *         required=true,
 *         type="integer"
 *     ),
 *     @SWG\Response(
 *         response=200,
 *         description="Course data.",
 *         @SWG\Schema(
 *            @SWG\Property(property="ID", type="integer"),
 *            @SWG\Property(property="TITLE", type="string"),
 *            @SWG\Property(property="DESCRIPTION", type="string"),
 *            @SWG\Property(property="EDMS", type="integer"),
 *            @SWG\Property(property="RESPONSIBLE", type="string"),
 *            @SWG\Property(property="CREATED", type="string"),
 *            @SWG\Property(property="UPDATED", type="string")
 *         )
 *     )
 * )
 */
$route->get(
  '/courses/:int',
  function($courseID) {
    Http::send_json(getCourse($courseID));
  }
);

/**
 * @SWG\Post(
 *     path="/courses",
 *     summary="Create new course",
 *     tags={"All", "Courses"},
 *     operationId="createCourse",
 *     produces={
 *         "application/json"
 *     },
 *     @SWG\Parameter(
 *         in="body",
 *         name="body",
 *         required=true,
 *         schema=@SWG\Schema(
 *            @SWG\Property(property="edmsId", type="integer")
 *         )
 *     ),
 *     @SWG\Response(
 *         response=200,
 *         description="Course has been created.",
 *         @SWG\Schema(
 *            @SWG\Property(property="ID", type="integer")
 *         )
 *     )
 * )
 */
$route->post(
  '/courses',
  function($params) {
    createCourse($params);
  }
);

/**
 * @SWG\Delete(
 *     path="/courses/{courseId}",
 *     summary="Delete course",
 *     tags={"All", "Courses"},
 *     operationId="deleteCourse",
 *     produces={
 *         "application/json"
 *     },
 *     @SWG\Parameter(
 *         in="path",
 *         name="courseId",
 *         required=true,
 *         type="integer"
 *     ),
 *     @SWG\Response(
 *         response=204,
 *         description="Successfully deleted course."
 *     )
 * )
 */
$route->delete(
  '/courses/:int',
  function($edmsID) {
    if (Utils::isMainAdmin()) {
      deleteCourse($edmsID);
    } else {
      Utils::sendPrivligesErr();
    }
  }
);

/**
 * @SWG\Get(
 *     path="/users/{userId}/courses",
 *     summary="Get user courses",
 *     tags={"All", "Users", "Courses"},
 *     operationId="getUserCoursesProgress",
 *     produces={
 *         "application/json"
 *     },
 *     @SWG\Parameter(
 *         in="path",
 *         name="userId",
 *         required=true,
 *         type="integer"
 *     ),
 *     @SWG\Response(
 *         response=200,
 *         description="List of user courses.",
 *         @SWG\Schema(
 *             type="array",
 *             @SWG\Items(
 *                 type="object",
 *                 @SWG\Property(property="user_id", type="integer"),
 *                 @SWG\Property(property="course_edms", type="integer"),
 *                 @SWG\Property(property="date_start", type="string"),
 *                 @SWG\Property(property="date_end", type="string"),
 *                 @SWG\Property(property="validated_by", type="string"),
 *             )
 *         )
 *     )
 * )
 */
$route->get(
  '/users/:string/courses',
  function($userId) {
    Http::send_json(getUserCoursesProgress($userId));
  }
);

/**
 * @SWG\Put(
 *     path="/users/{userId}/courses/{edmsId}/start",
 *     summary="Start user course",
 *     tags={"All", "Users", "Courses"},
 *     operationId="startUserCourse",
 *     produces={
 *         "application/json"
 *     },
 *     @SWG\Parameter(
 *         in="path",
 *         name="userId",
 *         required=true,
 *         type="string"
 *     ),
 *     @SWG\Parameter(
 *         in="path",
 *         name="edmsId",
 *         required=true,
 *         type="integer"
 *     ),
 *     @SWG\Response(
 *         response=200,
 *         description="User course started."
 *         )
 *     )
 * )
 */
$route->put(
  '/users/:string/courses/:int/start',
  function($userId, $edmsId) {
    Http::send_created(startUserCourse($userId, $edmsId));
  }
);

/**
 * @SWG\Put(
 *     path="/users/{userId}/courses/{edmsId}/complete",
 *     summary="Complete user course",
 *     tags={"All", "Users", "Courses"},
 *     operationId="completeUserCourse",
 *     produces={
 *         "application/json"
 *     },
 *     @SWG\Parameter(
 *         in="path",
 *         name="userId",
 *         required=true,
 *         type="string"
 *     ),
 *     @SWG\Parameter(
 *         in="path",
 *         name="edmsId",
 *         required=true,
 *         type="integer"
 *     ),
 *     @SWG\Response(
 *         response=200,
 *         description="User course completed."
 *         )
 *     )
 * )
 */
$route->put(
  '/users/:string/courses/:int/complete',
  function($userId, $edmsId) {
    Http::send_created(completeUserCourse($userId, $edmsId));
  }
);

/**
 * @SWG\Put(
 *     path="/users/{userId}/courses/{edmsId}/validate",
 *     summary="Validate user course",
 *     tags={"All", "Users", "Courses"},
 *     operationId="validateUserCourse",
 *     produces={
 *         "application/json"
 *     },
 *     @SWG\Parameter(
 *         in="path",
 *         name="userId",
 *         required=true,
 *         type="string"
 *     ),
 *     @SWG\Parameter(
 *         in="path",
 *         name="edmsId",
 *         required=true,
 *         type="integer"
 *     ),
 *     @SWG\Response(
 *         response=200,
 *         description="User course completed."
 *         )
 *     )
 * )
 */
$route->put(
  '/users/:string/courses/:int/validate',
  function($userId, $edmsId) {
    Http::send_created(validateUserCourse($userId, $edmsId));
  }
);

function deleteCourse($edmsID) {

  $dbh = DB::connect();

  $q = $dbh->prepare("DELETE FROM tr_course WHERE edms = :edmsID");
  $q->bindParam(':edmsID', $edmsID, PDO::PARAM_INT, 10);

  if ($q->execute()) {
    Http::send_deleted();
  }

  Http::send_error(
    'Error while deleting course.',
    $q->errorInfo()
  );

}

function getAllCourses(){

  $dbh = DB::connect();

  $q = $dbh->prepare("
  SELECT edms AS edmsId, COUNT(tr_training_course.TRAINING_ID) AS trainings
  FROM tr_course
  left join tr_training_course on tr_training_course.course_edms = tr_course.edms
  GROUP BY edms
  ");

  if ($q->execute()) {
    Http::send_json(Utils::array_change_key_case_recursive($q->fetchAll(PDO::FETCH_ASSOC)));
  } else {
    Http::send_error(
      'Error while getting courses.',
      $q->errorInfo()
    );
  }

}

function getCourse($courseID){

  $dbh = DB::connect();

  $q = $dbh->prepare("
    SELECT id, title, description, edms, responsible, TO_CHAR(created, 'YYYY-MM-DD') AS created, TO_CHAR(updated, 'YYYY-MM-DD') AS updated
    FROM tr_course
    WHERE id = :courseID
  ");
  $q->bindParam(':courseID', $courseID, PDO::PARAM_INT, 10);

  if ($q->execute()) {
    return $q->fetchAll(PDO::FETCH_ASSOC)[0];
  } else {
    Http::send_error(
      'Error while getting course.',
      $q->errorInfo()
    );
  }

}

function createCourse($params){

  $dbh = DB::connect();

  $edms = $params["edmsId"]?: Http::send_error("You need to provide an EDMS document.");

  $q = $dbh->prepare("
    INSERT INTO tr_course (edms)
    VALUES (:edms)
  ");
  $q->bindParam(':edms', $edms, PDO::PARAM_INT, 10);

  if ($q->execute()) {
    Http::send_created();
  } else {
    Http::send_error(
      'Error while creating new course.',
      $q->errorInfo()
    );
  }

}

function completeUserCourse($userId, $edmsId) {

  if ($userId !== User::current()) {
    Utils::sendPrivligesErr();
  }
  $dbh = DB::connect();

  $q = $dbh->prepare("
    UPDATE tr_user_course
    SET date_end = SYSDATE
    WHERE user_id = :userId
    AND course_edms = :edmsId
  ");
  $q->bindParam(':userId', $userId, PDO::PARAM_STR, 50);
  $q->bindParam(':edmsId', $edmsId, PDO::PARAM_INT, 10);

  if ($q->execute()) {
    $validators = getValidators($userId, $edmsId);
    foreach ($validators as $validatorId) {
      $validator = User::person($validatorId);
      $user = User::person(User::current());
      $course = (array)getEdmsDocument($edmsId);
      
      $email['to'] = $validator['email'];
      $email['title'] = 'Course for validation';
      $email['message'] = "Dear ".$validator['display_name'].",\r\n".
      $user['display_name']." have just finished course ".Utils::remove_accents($course["title"]).", and need validation.\r\n\r\n".
      "Go to https://op-webtools.web.cern.ch/training/#/ to validate.\r\n".
      "[Message sent automatically by the Training application. Please do not reply.]";
      
      Utils::sendEmail($email);
    }
    Http::send_created();
  } else {
    Http::send_error(
      'Error while adding user course.',
      $q->errorInfo()
    );
  }
}

function startUserCourse($userId, $edmsId) {
  if ($userId !== User::current()) {
    Utils::sendPrivligesErr();
  }

  $dbh = DB::connect();

  $q = $dbh->prepare("
    INSERT INTO tr_user_course (user_id, course_edms, date_start)
    VALUES (:userId, :edmsId, SYSDATE)
  ");
  $q->bindParam(':userId', $userId, PDO::PARAM_STR, 50);
  $q->bindParam(':edmsId', $edmsId, PDO::PARAM_INT, 10);

  if (!$q->execute()) {
    Http::send_error(
      'Error while validating user course.',
      $q->errorInfo()
    );
  }
}

function validateUserCourse($userId, $edmsId) {
  if (!canValidate($userId, $edmsId)) Http::send_error("You don't have the rights to validate this user's course");
  $current = User::current();

  $dbh = DB::connect();

  $q = $dbh->prepare("
    UPDATE tr_user_course
    SET validated_by = :currentId
    WHERE user_id = :userId
    AND course_edms = :edmsId
  ");
  $q->bindParam(':currentId', $current, PDO::PARAM_STR, 50);
  $q->bindParam(':userId', $userId, PDO::PARAM_STR, 50);
  $q->bindParam(':edmsId', $edmsId, PDO::PARAM_INT, 10);

  if (!$q->execute()) {
    Http::send_error(
      'Error while validating user course.',
      $q->errorInfo()
    );
  }
}

function canValidate($userId, $edmsId) {
    $current = User::current();
    return in_array($current, getValidators($userId, $edmsId));
  }

function getValidators($userId, $edmsId) {
    $q = DB::connect()->prepare("
      SELECT responsible 
      FROM tr_classroom_user_course 
      WHERE user_id = :userId
      AND course_edms = :edmsId
    ");
    $q->bindParam(':userId', $userId, PDO::PARAM_STR, 50);
    $q->bindParam(':edmsId', $edmsId, PDO::PARAM_INT, 10);

    if (!$q->execute()) Http::send_error( $q->errorInfo());
    $validators = implode(',', array_map(function($e) { return $e["RESPONSIBLE"]; }, $q->fetchAll()));
    return array_unique(explode(',', $validators));
  }


function getUserCoursesProgress($userId){
  $dbh = DB::connect();

  $query = "
    select a.user_id, classroom_id, a.course_edms, a.training_id, deadline, responsible, date_start, date_end, validated_by, training_pos from
    tr_classroom_user_course a
    left join  tr_user_course b on(a.course_edms = b.course_edms and a.user_id = b.user_id)
    
    left join tr_training_course c on (c.training_id = a.training_id and a.course_edms = c.course_edms)
    
    where a.user_id = :userID
    and c.course_edms is not null
    order by training_pos
  ";

  $q = $dbh->prepare($query);
  $userID = User::current();
  $q->bindParam(':userID', $userId, PDO::PARAM_STR, 20);

  if ($q->execute()) {
    $courses = Utils::array_change_key_case_recursive($q->fetchAll(PDO::FETCH_ASSOC));

    foreach ($courses as &$c) {
      if ($c["responsible"]) {
        $c["responsible"] = explode( ',', $c["responsible"] );
      } else {
        $c["responsible"] = [];
      }
      $midnight = strtotime('today midnight');

      if (!$c["date_start"] && !$c["date_end"] && strtotime($c["deadline"]) && strtotime($c["deadline"]) < $midnight) {
        $c["color"] = 'red';
        $c["status"] = 'Overdue';
      }
      elseif ($c["date_start"] && !$c["date_end"] && strtotime($c["deadline"]) && strtotime($c["deadline"]) < $midnight) {
        $c["color"] = '#FFC0CB'; // pink
        $c["status"] = 'Expired';
      }
      elseif ($c["date_start"] && !$c["date_end"]) {
        $c["color"] = 'blue';
        $c["status"] = 'Started';
      }
      elseif ($c["date_start"] && $c["date_end"] && (!$c["validated_by"] && $c["responsible"])) {
        $c["color"] = '#808000'; // olive
        $c["status"] = 'Validation';
      }
      elseif ($c["date_start"] && $c["date_end"] && ($c["validated_by"] || !$c["responsible"])) {
        $c["color"] = 'green';
        $c["status"] = 'Completed';
      }else {
        $c["color"] = 'orange';
        $c["status"] = 'New';
      }
    }

    return $courses;
  } else {
    Http::send_error(
      'Error while getting classrooms user progress.',
      $q->errorInfo()
    );
  }
}
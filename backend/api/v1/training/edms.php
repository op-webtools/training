<?php

// ROUTES --- --- ---

/**
 * @SWG\Get(
 *     path="/edms/folder/{folder}",
 *     summary="Get list of EDMS documents.",
 *     tags={"All", "Edms"},
 *     operationId="getEdmsDocuments",
 *     produces={
 *         "application/json"
 *     },
 *     @SWG\Parameter(
 *         in="path",
 *         name="folder",
 *         required=true,
 *         type="string"
 *     ),
 *     @SWG\Response(
 *         response=200,
 *         description="List of EDMS Documents.",
 *         @SWG\Schema(
 *             type="array",
 *             @SWG\Items(
 *                 type="object",
 *                 @SWG\Property(property="authors", type="string"),
 *                 @SWG\Property(property="creationDate", type="string"),
 *                 @SWG\Property(property="edmsId", type="integer"),
 *                 @SWG\Property(property="status", type="string"),
 *                 @SWG\Property(property="title", type="string"),
 *                 @SWG\Property(property="type", type="string"),
 *                 @SWG\Property(property="version", type="integer")
 *             )
 *         )
 *     )
 * )
 */

$route->get(
  '/edms/folder/:string',
  function($folder) {
    getEdmsDocuments($folder);
  }
);

/**
 * @SWG\Get(
 *     path="/edms/{id}",
 *     summary="Get EDMS document metadata.",
 *     tags={"All", "Edms"},
 *     operationId="getEdmsDocument",
 *     produces={
 *         "application/json"
 *     },
 *     @SWG\Parameter(
 *         in="path",
 *         name="id",
 *         required=true,
 *         type="integer"
 *     ),
 *     @SWG\Response(
 *         response=200,
 *         description="EDMS Document metadata.",
 *         @SWG\Schema(
 *             type="array",
 *             @SWG\Items(
 *                 type="object",
 *                 @SWG\Property(property="authors", type="string"),
 *                 @SWG\Property(property="context", type="string"),
 *                 @SWG\Property(property="creationDate", type="string"),
 *                 @SWG\Property(property="description", type="string"),
 *                 @SWG\Property(property="edmsId", type="integer"),
 *                 @SWG\Property(property="properties", type="object"),
 *                 @SWG\Property(property="releaseProc", type="string"),
 *                 @SWG\Property(property="status", type="string"),
 *                 @SWG\Property(property="title", type="string"),
 *                 @SWG\Property(property="type", type="string"),
 *                 @SWG\Property(property="url", type="string"),
 *                 @SWG\Property(property="version", type="integer"),
 *                 @SWG\Property(property="visibility", type="string")
 *             )
 *         )
 *     )
 * )
 */



$route->get(
  '/edms/:int',
  function($id) {
    Http::send_json(getEdmsDocument($id));
  }
);


function getEdmsDocuments($folder){

  $options = array(
	     'login' => 'TIOPWSC',
	     'password' => getenv("PASS_TIOPWSC"),
	);
	$client = new SoapClient('https://edms.cern.ch/ws/DocumentServiceT?wsdl', $options);
	$params = array(
		'username' => 'TIOPWSC',
		'objectType' => 'P',
		'objectEdmsId' => $folder
	);

	$soap_return = $client->getDocuments( $params);

  if (is_soap_fault($soap_return)) {
      http::send_error("SOAP Fault: (faultcode: {$soap_return->faultcode}, faultstring: {$soap_return->faultstring})", E_USER_ERROR);
  }
  $documents = $soap_return->result->document_list->document;

  $filtered = array_filter($documents, function($obj){
      return $obj->status === "Released";
  });

	http::send_json(array_values($filtered));
}


/**
 * @param int bp
 */
function getEdmsDocument($EDMS_Id) {
  $options = array(
	     'login' => 'TIOPWSC',
	     'password' => getenv("PASS_TIOPWSC"),
	);

	$client = new SoapClient('https://edms.cern.ch/ws/DocumentServiceT?wsdl', $options);

  $params = array('username' => 'TIOPWSC', 'edmsId' => $EDMS_Id);

	$soap_return = $client->getDocument( $params);

  if (is_soap_fault($soap_return)) {
      http::send_error("SOAP Fault: (faultcode: {$soap_return->faultcode}, faultstring: {$soap_return->faultstring})", E_USER_ERROR);
  }
	return $soap_return->result->document;
}

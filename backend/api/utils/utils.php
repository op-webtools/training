<?php
class Utils
{

  const DATE_FMT = 'yyyy-mm-dd"T"hh24:mi';

  public static function ifToPropagate() {
    return array_key_exists('propagate', $_GET)
      && $_GET['propagate'] === 'true';
  }

  public static function ifSimpleUser() {
    return array_key_exists('simple', $_GET)
      && $_GET['simple'] === 'true';
  }

  /**
   * @param string $userID
   */
  public static function checkIfUserExists($userID) {
    if (!User::person($userID))
      Http::send_error("Invalid user.");
  }

  /**
   * @param array $arr
   * @param const [$case] (CASE_LOWER|CASE_UPPER)
   */
  public static function array_change_key_case_recursive($arr, $case=CASE_LOWER) {
    return array_map(function($item) use ($case) {
      if (is_array($item))
      $item = self::array_change_key_case_recursive($item, $case);
      return $item;
    }, array_change_key_case($arr, $case));
  }

  public static function isMainAdmin() {
    $q = DB::connect()->prepare("
      SELECT classroom_id FROM tr_classroom_admin WHERE admin = :userId
    ");
    $usr = User::current();
    $q->bindParam(":userId", $usr);

    if (!$q->execute()) Http::send_error( $q->errorInfo());
    $res = $q->fetchAll();

    return count($res) > 0;
  }

  public static function isTrainingAdmin($trainingId) {
    $q = DB::connect()->prepare("
      SELECT id FROM tr_training WHERE id = :trainingId and responsible = :userId
    ");

    $q->bindParam(":trainingId", $trainingId);
    $usr = User::current();
    $q->bindParam(":userId", $usr);

    if (!$q->execute()) Http::send_error( $q->errorInfo());
    $res = $q->fetchAll();

    return count($res) > 0;
  }

  public static function isGroupAdmin($groupId) {
    $q = DB::connect()->prepare("
      SELECT id FROM tr_group WHERE id = :groupId and admin = :userId
    ");

    $q->bindParam(":groupId", $groupId);
    $usr = User::current();
    $q->bindParam(":userId", $usr);

    if (!$q->execute()) Http::send_error( $q->errorInfo());
    $res = $q->fetchAll();

    return count($res) > 0;
  }

  public static function isClassroomAdmin($classroomId) {
    $q = DB::connect()->prepare("
      SELECT admin FROM TR_CLASSROOM_ADMIN WHERE CLASSROOM_ID = :classroomId
    ");
    $q->bindParam(":classroomId", $classroomId);

    if (!$q->execute()) Http::send_error( $q->errorInfo());

    $admins = array_map(
      function($r) { return $r["ADMIN"]; },
      $q->fetchAll()
    );

    return in_array(User::current(), $admins);
  }

  public static function isClassroomVisitor($classroomId) {
    $q = DB::connect()->prepare("
      SELECT user_id FROM tr_classroom_visitor WHERE classroom_id = :classroomId
    ");
    $q->bindParam(":classroomId", $classroomId);

    if (!$q->execute()) Http::send_error( $q->errorInfo());

    $visitors = array_map(
      function($r) { return $r["USER_ID"]; },
      $q->fetchAll()
    );

    return in_array(User::current(), $visitors)
      ? true
      : count(array_filter($visitors, function($v) {
        return strpos($v, '@') !== false
          ? User::is_egroup_member(ltrim($v, '@'))
          : false;
      })) > 0;
  }

  public static function sendPrivligesErr() {
    Http::send_error("You need admin privliges to perform that operation");
  }
  
  public static function whiteListParameters($params, $keys){
    foreach (array_keys($params) as $k) {
      if (!in_array($k, $keys)) {
        unset($params[$k]);
      }
    }
    return $params;
  }

  public static function remove_accents ($str) {
    $unwanted_array = array(
      'Š'=>'S', 'š'=>'s', 'Ž'=>'Z', 'ž'=>'z', 'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E',
      'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U',
      'Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss', 'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'ç'=>'c',
      'è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o',
      'ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'þ'=>'b', 'ÿ'=>'y' );
    return strtr( $str, $unwanted_array );
  }

  public static function sendEmail($params) {
    // if ('@DOCS_HOST' != 'op-webtools.web.cern.ch') return;
     // <-- FOR DEVELOPMENT ONLY

    $user = User::person(User::current());
    if (!$user) Http::send_error("Not a valid user");

    $message = wordwrap($params['message'], 75, "\r\n");
    $headers = 'From: '.$user['email']."\r\n" .
              'Reply-To: '.$user['email']."\r\n" .
              'X-Mailer: PHP/'.phpversion();
    mail($params['to'], $params['title'], $message, $headers);
  }
}

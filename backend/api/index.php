<?php

/**
 * @SWG\Swagger(
 *     host="@DOCS_HOST",
 *     basePath="/training/backend/api/v1",
 *     schemes={"https"},
 *     @SWG\Info(
 *         version="0.1",
 *         title="raining API"
 *     )
 * )
 */

error_reporting(E_ALL);
ini_set('display_errors', 0);
ini_set('log_errors', 1);
ini_set('error_log', '/tmp/php.log');

require_once __DIR__.'/../assets/rest-handler/RequestHandler.class.php';
require_once __DIR__.'/../assets/http-wrapper/Http.class.php';
require_once __DIR__.'/../assets/db/connection.php';
require_once __DIR__.'/../assets/db/db.php';
require_once __DIR__.'/utils/utils.php';

$db = dbConnection(getenv("DB_USER"));
DB::config($db);

$route = new RequestHandler('/v1');

// START import API endpoint files
require_once __DIR__."/v1/training/auth.php";
require_once __DIR__."/v1/training/classrooms.php";
require_once __DIR__."/v1/training/courses.php";
require_once __DIR__."/v1/training/edms.php";
require_once __DIR__."/v1/training/groups.php";
require_once __DIR__."/v1/training/ip.php";
require_once __DIR__."/v1/training/trainings.php";
require_once __DIR__."/v1/training/email.php";
// END import API endpoint files

$route->handle();
